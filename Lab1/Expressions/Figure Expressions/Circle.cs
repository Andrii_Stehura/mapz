﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace FiguresInterpreter
{
	class Circle: Drawable
	{
		public int Radius{ get; set; }
		public Circle()
		{
			Radius = 0;
		}
		public override void Draw(DrawingGroup dg)
		{
			if (!CheckProperties())
				throw (new Exception("There are unset properties in a circle object."));

			dg.Children.Remove(Figure);
			Initialize();
			dg.Children.Add(Figure);
		}

		public override void SetProperty(string name, object value)
		{
			switch (name)
			{
				case "centerY":
					{
						if (value is int v)
						{
							StartPoint = new Point(StartPoint.X, v);
							return;
						}
						break;
					}
				case "centerX":
					{
						if (value is int v)
						{
							StartPoint = new Point(v, StartPoint.Y);
							return;
						}
						break;
					}
				case "color":
					{
						if (value is Brush b)
						{
							this.Brush = b;
							return;
						}
						break;
					}
				case "radius":
					{
						if (value is int v)
						{
							Radius = v;
							return;
						}
						break;
					}
				default:
					{
						throw (new Exception($"Can not find property '{name}' in a circle object."));
					}
			}
			throw (new Exception($"Can not set property '{name}' to type" +
								$" of {value.GetType().Name} in a circle object."));
		}

		public override object GetProperty(string name)
		{
			switch (name)
			{
				case "startY":
					{
						return StartPoint.Y;
					}
				case "startX":
					{
						return StartPoint.X;
					}
				case "color":
					{
						return this.Brush;
					}
				case "radius":
					{
						return Radius;
					}
			}
			throw (new Exception($"Can not find property '{name}' in a circle object."));
		}

		protected override bool CheckProperties()
		{
			bool IsReady = false;
			if (Radius > 0)
				IsReady = true;
			return IsReady;
		}

		public override void Initialize()
		{
			EllipseGeometry eg = new EllipseGeometry();
			eg.Center = StartPoint;
			eg.RadiusX = Radius;
			eg.RadiusY = Radius;
			Figure.Geometry = eg;
			Figure.Brush = Brush;
		}

		public override string ToString()
		{	
			string str = $"{{centerX = {StartPoint.X} centerY = {StartPoint.Y} " +
				$"radius = {Radius} color = {ColorExpression.GetColorName(Brush)}}}";
			return str;
		}
	}
}
