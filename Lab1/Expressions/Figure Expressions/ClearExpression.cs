﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class ClearExpression:IExpression, IPrioriable
	{
		public object Interpret(Context c)
		{
			c.drawingGroup.Children.Clear();
			return null;
		}

		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.Low;
		}
	}
}
