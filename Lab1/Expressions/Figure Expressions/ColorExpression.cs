﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;

namespace FiguresInterpreter
{
	class ColorExpression:IExpression
	{
		Brush b;
		public ColorExpression(string color)
		{
			switch (color)
			{
				case ("GREEN"):
					{
						b = Brushes.Green;
						break;
					}
				case ("BLACK"):
					{
						b = Brushes.Black;
						break;
					}
				case ("YELLOW"):
					{
						b = Brushes.Yellow;
						break;
					}
				case ("RED"):
					{
						b = Brushes.Red;
						break;
					}
				case ("WHITE"):
					{
						b = Brushes.White;
						break;
					}
				case ("BLUE"):
					{
						b = Brushes.Blue;
						break;
					}
				default:
					{
						throw (new Exception($"Undefined color '{color}'"));
					}
			}
		}

		public object Interpret(Context c)
		{	
			return b;
		}

		public static string GetColorName(Brush brush)
		{
			switch (brush.ToString())
			{
				case ("#FF008000"):
					{
						return "GREEN";
					}
				case ("#FF000000"):
					{
						return "BLACK";
					}
				case ("#FFFFFF00"):
					{
						return "YELLOW";
					}
				case ("#FFFF0000"):
					{
						return "RED";
					}
				case ("#FFFFFFFF"):
					{
						return "WHITE";
					}
				case ("#FF0000FF"):
					{
						return "BLUE";
					}
			}
			return null;
		}
	}
}
