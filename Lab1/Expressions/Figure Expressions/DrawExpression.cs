﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class DrawExpression: UnaryExpression, IExpression, IPrioriable
	{
		public object Interpret(Context c)
		{
			object obj = firstExpression.Interpret(c);
			if (obj is Drawable == false)
				throw (new Exception($"Can not draw object of type{obj.GetType().Name}"));

			((Drawable)obj).Draw(c.drawingGroup);
			return obj;
		}

		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.Low;
		}
	}
}
