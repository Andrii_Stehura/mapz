﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace FiguresInterpreter
{
	abstract class Drawable
	{
		private Point startPoint;
		public Pen Pen { get; private set; }
		public Brush Brush { get; set; }
		
		public Point StartPoint 
		{
			get
			{
				return startPoint;
			}
			set
			{
				startPoint.X = value.X;
				startPoint.Y = value.Y;
			}
		}

		public GeometryDrawing Figure { get; protected set; }
		protected Drawable()
		{
			Pen = new Pen(Brushes.Black, 1);
			Brush = Brushes.White;
			Figure = new GeometryDrawing();
			Figure.Pen = Pen;
			Figure.Brush = Brush;
		}
		public abstract void Draw(DrawingGroup dg);
		public abstract void SetProperty(string name, object value);
		public abstract object GetProperty(string name);
		protected abstract bool CheckProperties();
		public abstract void Initialize();
	}
}
