﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;

namespace FiguresInterpreter
{
	class GeometricalResult : Drawable
	{
		public GeometricalResult(GeometryDrawing gd)
		{
			Figure = gd;
		}

		public override void Draw(DrawingGroup dg)
		{
			dg.Children.Add(Figure);
		}

		public override object GetProperty(string name)
		{
			return null;
		}

		public override void Initialize()
		{
			return;
		}

		public override void SetProperty(string name, object value)
		{
			return;
		}

		protected override bool CheckProperties()
		{
			return true;
		}
	}
}
