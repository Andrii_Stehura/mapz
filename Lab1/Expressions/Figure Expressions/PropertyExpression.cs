﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class PropertyExpression: IExpression, IAssignable, IPrioriable
	{
		public string AssignableName { get; set; }
		public string PropertyName { get; set; }
		public PropertyExpression(string propLexem):base()
		{
			string[] tokens = propLexem.Split('.');
			if (tokens.Length != 2)
				throw (new Exception("Wrong property syntax."));
			AssignableName = tokens[0];
			PropertyName = tokens[1];
		}

		public object Interpret(Context c)
		{
			object obj = c.GetVariable(AssignableName);
			if (obj is IExpression e)
				obj = e.Interpret(c);
			if (obj is Drawable d)
			{
				obj = d.GetProperty(PropertyName);
			}
			else
				throw (new Exception($"Type '{obj.GetType().Name}' does not has properties."));
			return obj;
		}

		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.BelowNormal;
		}
	}
}
