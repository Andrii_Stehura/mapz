﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace FiguresInterpreter
{
	class Rectangle: Drawable
	{
		public int Height { get; set; }
		public int Width { get; set; }

		public Rectangle(): base()
		{
			Height = 0;
			Width = 0;
		}

		public override void Draw(DrawingGroup dg)
		{
			if (!CheckProperties())
				throw (new Exception("There are unset properties in a rectangle object."));

			dg.Children.Remove(Figure);
			Initialize();
			dg.Children.Add(Figure);
		}

		public override void SetProperty(string name, object value)
		{
			switch (name)
			{
				case "startY":
					{
						if (value is int v)
						{
							StartPoint = new Point(StartPoint.X, v);
							return;
						}
						break;
					}
				case "startX":
					{
						if (value is int v)
						{
							StartPoint = new Point(v, StartPoint.Y);
							return;
						}
						break;
					}
				case "color":
					{
						if (value is Brush b)
						{
							this.Brush = b;
							return;
						}
						break;
					}
				case "height":
					{
						if (value is int v)
						{
							Height = v;
							return;
						}
						break;
					}
				case "width":
					{
						if (value is int v)
						{
							Width = v;
							return;
						}
						break;
					}
				default:
					{
						throw (new Exception($"Can not find property '{name}' in a rectangle object."));
					}
			}
			throw (new Exception($"Can not set property '{name}' to type" +
								$" of {value.GetType().Name} in a rectangle object."));
		}

		public override object GetProperty(string name)
		{
			switch (name)
			{
				case "startY":
					{
						return StartPoint.Y;
					}
				case "startX":
					{
						return StartPoint.X;
					}
				case "color":
					{
						return this.Brush;
					}
				case "height":
					{
						return Height;
					}
				case "width":
					{
						return Width;
					}
			}
			throw (new Exception($"Can not find property '{name}' in a rectangle object."));
		}

		protected override bool CheckProperties()
		{
			bool IsReady = false;
			if (Width > 0 && Height > 0)
				IsReady = true;
			return IsReady;
		}

		public override void Initialize()
		{
			RectangleGeometry rg = new RectangleGeometry();
			Rect rect = new Rect(new Size(Width, Height));
			rect.X = StartPoint.X;
			rect.Y = StartPoint.Y;
			rg.Rect = rect;
			Figure.Geometry = rg;
			Figure.Brush = Brush;
		}

		public override string ToString()
		{
			string str = $"{{startX = {StartPoint.X} startY = {StartPoint.Y} " +
				$"width = {Width} height = {Height}" +
					$"color = {ColorExpression.GetColorName(Brush)}}}";
			return str;
		}
	}
}
