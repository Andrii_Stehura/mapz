﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace FiguresInterpreter
{
	class Triangle: Drawable
	{
		public int SideLength { get; set; }
		public Triangle():base(){}

		public override void Draw(DrawingGroup dg)
		{
			if (!CheckProperties())
				throw (new Exception("There are unset properties in a triangle object."));

			dg.Children.Remove(Figure);
			Initialize();
			dg.Children.Add(Figure);
		}

		public override void SetProperty(string name, object value)
		{
			switch (name)
			{
				case "startY":
					{
						if (value is int v)
						{
							StartPoint = new Point(StartPoint.X, v);
							return;
						}
						break;
					}
				case "startX":
					{
						if (value is int v)
						{
							StartPoint = new Point(v, StartPoint.Y);
							return;
						}
						break;
					}
				case "color":
					{
						if (value is Brush b)
						{
							this.Brush = b;
							return;
						}
						break;
					}
				case "side":
					{
						if (value is int v)
						{
							SideLength = v;
							return;
						}
						break;
					}
				default:
					{
						throw (new Exception($"Can not find property '{name}' in a triangle object."));
					}
			}
			throw (new Exception($"Can not set property '{name}' to type" +
								$" of {value.GetType().Name} in a triangle object."));
		}

		public override object GetProperty(string name)
		{
			switch (name)
			{
				case "startY":
					{
						return StartPoint.Y;
					}
				case "startX":
					{
						return StartPoint.X;
					}
				case "color":
					{
						return this.Brush;
					}
				case "side":
					{
						return SideLength;
					}
			}
			throw (new Exception($"Can not find property '{name}' in a triangle object."));
		}

		protected override bool CheckProperties()
		{
			bool IsReady = false;
			if (SideLength > 0)
				IsReady = true;
			return IsReady;
		}

		public override void Initialize()
		{
			PathGeometry pg = new PathGeometry();
			PathFigure pathFigure = new PathFigure();
			pathFigure.IsClosed = true;
			pathFigure.StartPoint = StartPoint;

			LineSegment mainLine = new LineSegment();
			mainLine.Point = new Point(pathFigure.StartPoint.X + SideLength,
				pathFigure.StartPoint.Y);

			LineSegment nextLine = new LineSegment();
			nextLine.Point = new Point(pathFigure.StartPoint.X + SideLength / 2,
				pathFigure.StartPoint.Y - SideLength / 2);

			pathFigure.Segments.Add(mainLine);
			pathFigure.Segments.Add(nextLine);

			PathFigureCollection pathFigures = new PathFigureCollection();
			pathFigures.Add(pathFigure);
			pg.Figures = pathFigures;

			Figure.Geometry = pg;
			Figure.Brush = Brush;
		}

		public override string ToString()
		{
			string str = $"{{startX = {StartPoint.X} startY = {StartPoint.Y} " +
				$"side = {SideLength} color = {ColorExpression.GetColorName(Brush)}}}";
			return str;
		}
	}
}
