﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;

namespace FiguresInterpreter
{
	class XorExpression : BinaryExpression, IExpression, IPrioriable
	{
		public XorExpression() : base() { }

		public object Interpret(Context c)
		{
			object first = firstExpression.Interpret(c);
			object second = secondExpression.Interpret(c);
			if (first is Drawable f && second is Drawable s)
			{
				if (f.Figure.Geometry == null)
					f.Initialize();

				if (s.Figure.Geometry == null)
					s.Initialize();

				PathGeometry pg = Geometry.Combine(f.Figure.Geometry, s.Figure.Geometry,
					GeometryCombineMode.Xor, null);
				GeometryDrawing gd = new GeometryDrawing();
				gd.Geometry = pg;
				gd.Pen = f.Pen;
				gd.Brush = f.Brush;
				GeometricalResult gr = new GeometricalResult(gd);
				gr.Brush = gd.Brush;
				return gr;
			}
			throw (new Exception($"Can not xor objects of type " +
				$"{first.GetType().Name} and {second.GetType().Name}"));
		}

		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.Normal;
		}
	}
}
