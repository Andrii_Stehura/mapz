﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace FiguresInterpreter
{
	interface IExpression
	{
		object Interpret(Context context);
	}
}
