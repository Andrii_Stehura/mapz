﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	interface IPrioriable
	{
		ExpressionPriority GetPriority();
	}
}
