﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class AndExpression: BinaryExpression, IExpression, IPrioriable
	{

		public AndExpression():base(){}

		public object Interpret(Context c)
		{
			object firstRes = firstExpression.Interpret(c);
			int result = 0;
			if (firstRes != null)
				if (firstRes is int fr)
					if (fr > 0)
					{
						object secondRes = secondExpression.Interpret(c);
						if (secondRes != null)
							if (secondRes is int sr)
								if (sr > 0)
									result = 1;
					}
			return result;
		}

		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.Low;
		}


	}
}
