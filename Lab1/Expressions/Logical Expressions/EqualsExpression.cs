﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class EqualsExpression: BinaryExpression, IExpression, IPrioriable
	{
		public EqualsExpression(): base(){}

		public object Interpret(Context context)
		{
			object first = firstExpression.Interpret(context);
			object second = secondExpression.Interpret(context);
			if (first is int f && second is int s)
				return (f == s) ? 1 : 0;
			throw (new Exception($"Invalid operand in {first.GetType()} == {second.GetType()}"));
		}

		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.BelowNormal;
		}
	}
}
