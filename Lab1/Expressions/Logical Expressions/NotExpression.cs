﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class NotExpression : UnaryExpression, IExpression, IPrioriable
	{
		public NotExpression() : base() { }

		public object Interpret(Context c)
		{
			int result = 1;
			object firstRes = firstExpression.Interpret(c);
			if (firstRes != null)
				if (firstRes is int fr)
					if (fr > 0)
						result = 0;
			return result;
		}

		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.Normal;
		}
	}
}
