﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class OrExpression : BinaryExpression, IExpression, IPrioriable
	{
		public OrExpression() : base() { }

		public object Interpret(Context c)
		{
			int result = 0;
			object firstRes = firstExpression.Interpret(c);
			if (firstRes != null)
				if (firstRes is int fr)
					if (fr > 0)
						return 1;

			object secondRes = secondExpression.Interpret(c);
			if (secondRes != null)
				if (secondRes is int sr)
					if (sr > 0)
						result = 1;

			return result;
		}

		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.Low;
		}
	}
}
