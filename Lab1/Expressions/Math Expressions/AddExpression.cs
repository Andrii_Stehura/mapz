﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class AddExpression: BinaryExpression, IExpression, IPrioriable
	{
		public AddExpression():base(){}
		public AddExpression(IExpression leftExpr, IExpression rightExpr):base()
		{
			firstExpression = leftExpr;
			secondExpression = rightExpr;
		}

		public object Interpret(Context context)
		{
			object first = firstExpression.Interpret(context);
			object second = secondExpression.Interpret(context);
			if (first is int f && second is int s)
				return f + s;
			throw (new Exception($"Invalid operand in {first.GetType()} + {second.GetType()}"));
		}

		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.Normal;
		}
	}
}
