﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class ConstantExpression:IExpression
	{
		int value;
		public ConstantExpression(string toParse)
		{
			if (!int.TryParse(toParse, out value))
				throw (new Exception($"Can not parse {toParse}"));
		}

		public object Interpret(Context c)
		{
			return value;
		}
	}
}
