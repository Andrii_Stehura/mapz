﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class DivisionExpression : BinaryExpression, IExpression, IPrioriable
	{
		public DivisionExpression():base(){}
		public DivisionExpression(IExpression left, IExpression right) : base()
		{
			firstExpression = left;
			secondExpression = right;
		}

		public object Interpret(Context c)
		{
			object first = firstExpression.Interpret(c);
			object second = secondExpression.Interpret(c);
			if (first is int f && second is int s)
				if (s != 0)
				{
					return f / s;
				}
				else
					throw (new Exception("Division by 0."));

			throw (new Exception($"Invalid operand in {first.GetType()} / {second.GetType()}"));
		}

		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.AboveNormal;
		}
	}
}
