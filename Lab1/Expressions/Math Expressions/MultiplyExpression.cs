﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class MultiplyExpression: BinaryExpression, IExpression, IPrioriable
	{
		public MultiplyExpression() : base() { }
		public MultiplyExpression(IExpression left, IExpression right) : base()
		{
			firstExpression = left;
			secondExpression = right;
		}

		public object Interpret(Context c)
		{
			object first = firstExpression.Interpret(c);
			object second = secondExpression.Interpret(c);
			if (first is int f && second is int s)
				return f * s;
			throw (new Exception($"Invalid operand in {first.GetType()} * {second.GetType()}"));
		}

		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.AboveNormal;
		}
	}
}
