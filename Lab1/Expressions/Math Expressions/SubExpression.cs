﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class SubExpression: BinaryExpression, IExpression, IPrioriable
	{
		public SubExpression():base(){}
		public SubExpression(IExpression leftExpression, IExpression rightExpression):base()
		{
			this.firstExpression = leftExpression;
			this.secondExpression = rightExpression;
		}

		public object Interpret(Context c)
		{
			object first = firstExpression.Interpret(c);
			object second = secondExpression.Interpret(c);
			if (first is int f && second is int s)
				return f - s;
			throw (new Exception($"Invalid operand in {first.GetType()} - {second.GetType()}"));
		}

		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.Normal;
		}
	}
}
