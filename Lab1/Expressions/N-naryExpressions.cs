﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	abstract class UnaryExpression
	{
		public IExpression firstExpression { get; set; }

		public UnaryExpression()
		{
			firstExpression = null;
		}
	}

	abstract class BinaryExpression : UnaryExpression
	{
		public IExpression secondExpression { get; set; }

		public BinaryExpression() : base()
		{
			secondExpression = null;
		}
	}
}
