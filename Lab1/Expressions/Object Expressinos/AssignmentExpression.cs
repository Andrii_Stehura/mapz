﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	interface IAssignable
	{
		string AssignableName { get; set; }
	}

	class AssignmentExpression : BinaryExpression, IExpression, IPrioriable
	{
		public AssignmentExpression() : base() { }

		public object Interpret(Context c)
		{
			if (firstExpression is IAssignable i)
				SetValue(c, i);
			else if (firstExpression.Interpret(c) is IAssignable a)
			{
				SetValue(c, a);
			}
			else
				throw (new Exception($"Can not assign result of {secondExpression.GetType().Name}" +
					$" to {firstExpression.GetType().Name}"));
			return firstExpression;
		}

		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.BelowNormal;
		}

		private void SetValue(Context c, IAssignable i)
		{
			object value = secondExpression.Interpret(c);
			if (value is IExpression expr)
				value = expr.Interpret(c);
			if (firstExpression is PropertyExpression pe)
			{
				object obj = c.GetVariable(pe.AssignableName);
				if (obj is IExpression expression)
					obj = expression.Interpret(c);
				if (obj is Drawable d)
				{
					d.SetProperty(pe.PropertyName, value);
				}
				else
					throw (new Exception($"Variable '{pe.AssignableName}' of type " +
						$"'{obj.GetType().Name}' does not has properties."));
			}
			else
			{
				ObjectExpression oe = (ObjectExpression)c.GetVariable(i.AssignableName);
				if (oe.Value is Drawable || value is Drawable)
					throw (new Exception("Can not use assignment to drawable objects. " +
						"Use properties instead."));
				c.SetVariable(i.AssignableName, value);
			}
		}
	}
}
