﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;

namespace FiguresInterpreter
{
	class Context
	{
        Dictionary<string, ObjectExpression> objects;
        Dictionary<string, int> nameFrequency;
        public DrawingGroup drawingGroup;
        public Context()
        {
            objects = new Dictionary<string, ObjectExpression>();
            drawingGroup = new DrawingGroup();
            nameFrequency = new Dictionary<string, int>();
        }

        public Context(Context c)
        {
            this.objects = new Dictionary<string, ObjectExpression>(c.objects);
        }
        
        public object GetVariable(string name)
        {
            if(objects.ContainsKey(name))
                return objects[name];

            throw (new Exception($"Object {name} does not exsist!"));
        }

        public void SetVariable(string name, object value)
        {
            if (objects.ContainsKey(name))
                objects[name].Value = value;
            else
                objects.Add(name, new ObjectExpression(name, value));
        }

        public string[] GetObjectsToString()
        {
            string[] res = new string[objects.Count];
            int i = 0;
            foreach (var x in objects)
            { 
                res[i++] = $"{x.Value.Value.GetType().Name} {x.Key} {x.Value.Value}";
            }
            return res;
        }

        public void RemoveVariable(string name)
        {
            if (objects.ContainsKey(name))
                objects.Remove(name);
        }

        public bool ContainsName(string name)
        {
            if (objects.ContainsKey(name))
                return true;
            return false;
        }

        /*public void AddName(string name)
        {
            if (nameFrequency.ContainsKey(name))
                ++nameFrequency[name];
            else
                nameFrequency.Add(
        }*/
    }
}
