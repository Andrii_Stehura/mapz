﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class DeleteExpression: UnaryExpression, IExpression, IPrioriable
	{
		public DeleteExpression() : base() { }

		public object Interpret(Context context)
		{
			if (firstExpression is ObjectExpression oe)
			{
				context.RemoveVariable(oe.AssignableName);
			}
			return null;
		}

		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.Normal;
		}
	}
}
