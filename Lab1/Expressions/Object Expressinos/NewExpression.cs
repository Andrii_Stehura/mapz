﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class NewExpression: BinaryExpression, IExpression, IPrioriable
	{
		public NewExpression() : base() { }

		public object Interpret(Context c)
		{
			if (firstExpression == null || firstExpression.GetType() != typeof(TypeExpression))
				throw (new Exception("Eror when creating an object - wrong type."));
			if (secondExpression == null || secondExpression .GetType() != typeof(ObjectExpression))
				throw (new Exception("Eror when creating an object - wrong name."));
			return CreateObject(c);
		}

		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.Normal;
		}

		private IExpression CreateObject(Context c)
		{
			ObjectExpression obj = null;
			string type = (string)firstExpression.Interpret(c);
			string name = ((ObjectExpression)secondExpression).AssignableName;
			switch (type)
			{
				case "variable":
					{
						obj = new VariableExpression();
						obj.Value = 0;
						break;
					}
				case "triangle":
					{
						obj = new ObjectExpression();
						obj.Value = new Triangle();
						break;
					}
				case "rectangle":
					{
						obj = new ObjectExpression();
						obj.Value = new Rectangle();
						break;
					}
				case "circle":
					{
						obj = new ObjectExpression();
						obj.Value = new Circle();
						break;
					}
			}
			if (obj == null || c.ContainsName(name))
				throw (new Exception($"Can not create object of type '{type}' with name '{name}'."));
			
			obj.AssignableName = name;
			c.SetVariable(name, obj.Value);
			return obj;
		}
	}
}
