﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class ObjectExpression: IExpression, IAssignable
	{
		public string AssignableName { get; set; }
		public object Value { get; set; }
		public ObjectExpression(){}
		public ObjectExpression(string name) 
		{
			AssignableName = name;
		}
		public ObjectExpression(object value) 
		{
			Value = value;
		}
		public ObjectExpression(string name, object value)
		{
			AssignableName = name;
			Value = value;
		}

		public object Interpret(Context context)
		{
			ObjectExpression oe = (ObjectExpression)context.GetVariable(AssignableName);
			return oe.Value;
		}
	}
}
