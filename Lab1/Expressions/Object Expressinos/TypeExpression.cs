﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class TypeExpression : IExpression
	{
		string type;
		public TypeExpression(string type)
		{
			this.type = type;
		}

		public object Interpret(Context c)
		{
			return type;
		}
	}
}
