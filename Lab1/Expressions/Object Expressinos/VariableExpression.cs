﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class VariableExpression: ObjectExpression, IExpression
	{
		public VariableExpression():base(){}
		public VariableExpression(string name):base(name){}

		public new object Interpret(Context c)
		{
			return c.GetVariable(AssignableName);
		}
	}
}
