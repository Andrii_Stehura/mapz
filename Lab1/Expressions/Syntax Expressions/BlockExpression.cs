﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class BlockExpression:IExpression, IPrioriable
	{
		List<IExpression> expressions;
		public BlockExpression()
		{
			expressions = new List<IExpression>();
		}

		public BlockExpression(List<IExpression> list)
		{
			expressions = list;
		}

		public BlockExpression(BlockExpression be): this(be.GetExpressions())
		{}

		public object Interpret(Context c)
		{
			object lastResult = null;
			foreach (var x in expressions)
				lastResult = x.Interpret(c);
			return lastResult;
		}

		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.VeryHigh;
		}

		public void AddExpression(IExpression expression)
		{
			expressions.Add(expression);
		}

		public List<IExpression> GetExpressions()
		{
			return new List<IExpression>(expressions);
		}
	}
}
