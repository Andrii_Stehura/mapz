﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class BracketExpression : IExpression, IPrioriable
	{
		public object Interpret(Context context)
		{
			return null;
		}

		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.AboveNormal;
		}
	}

	class LeftBracketExpression: BracketExpression
	{
		public LeftBracketExpression() : base() { }
	}

	class RightBracketExpression : BracketExpression
	{
		public RightBracketExpression() : base() { }
	}

	class CurlyLeftBracketExpression : BracketExpression
	{
		public CurlyLeftBracketExpression() : base() { }
	}
	class CurlyRightBracketExpression : BracketExpression
	{
		public CurlyRightBracketExpression() : base() { }
	}
}
