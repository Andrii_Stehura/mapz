﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	abstract class ConditionalExpression
	{
		public IExpression condition { get; set; }
		public IExpression positiveCase { get; set; }
		protected ConditionalExpression()
		{
			condition = null;
			positiveCase = null;
		}
	}
}
