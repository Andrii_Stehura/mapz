﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class IfExpression : ConditionalExpression, IExpression, IPrioriable
	{
		public IExpression negativeCase { get; set; }

		public IfExpression()
		{
			condition = null;
			positiveCase = null;
			negativeCase = null;
		}

		public object Interpret(Context c)
		{
			if (positiveCase is BlockExpression == false)
				throw (new Exception("'if' operator needs block expression."));
			object result = condition.Interpret(c);
			int r = 0;
			if (result != null)
				if (result is int res)
					r = res;
			if (r > 0)
			{
				result = positiveCase.Interpret(c);
			}
			else if (negativeCase != null)
			{

				result = negativeCase.Interpret(c);
			}
			return result;
		}

		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.High;
		}

	}

	class ElseExpression : IExpression
	{
		public IExpression elseCase { get; set; }
		public ElseExpression()
		{
			elseCase = null;
		}

		public object Interpret(Context c)
		{
			if (elseCase is BlockExpression == false)
				throw (new Exception("'else' operator needs block expression."));
			return elseCase.Interpret(c);
		}

		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.High;
		}
	}
}
