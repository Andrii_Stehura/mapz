﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	class WhileExpression: ConditionalExpression, IExpression, IPrioriable
	{
		public WhileExpression()
		{
			condition = null;
			positiveCase = null;
		}

		public object Interpret(Context c)
		{
			if(positiveCase is BlockExpression == false)
				throw (new Exception("'while' operator needs block expression."));

			bool whileCondition = true;
			object result = null;
			int r = 0;
			while (whileCondition)
			{
				result = condition.Interpret(c);
				if (result is ObjectExpression oe)
					result = oe.Interpret(c);
				if (result != null)
					if (result is int res)
						r = res;
				if (r > 0)
				{
					result = positiveCase.Interpret(c);
				}
				else
					whileCondition = false;
			}
			return result;
		}
		public ExpressionPriority GetPriority()
		{
			return ExpressionPriority.High;
		}
	}
}
