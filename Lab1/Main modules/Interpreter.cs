﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace FiguresInterpreter
{
	class Interpreter
	{
		string input;
		public Interpreter(string input)
		{
			this.input = input;
		}

		public TreeViewItem[] Interpret(Context c)
		{
			string[][] tokensArr = Lexer.Tokenize(input);
			BlockExpression be = new BlockExpression();
			bool wasCurlyLeft = false;
			for (int i = 0; i < tokensArr.Length; ++i)
			{
				IExpression expression = Parse(tokensArr[i]);
				if (expression is ConditionalExpression ce)
				{
					HandleConditional(ce, tokensArr, ref i);
					if (i + 1 >= tokensArr.Length)
					{
						be.AddExpression((IExpression)ce);
						break;
					}
				}

				if (expression is ElseExpression)
				{
					throw (new Exception("An 'else' expression is used without 'if' before it."));
				}

				if (expression is CurlyLeftBracketExpression)
				{
					wasCurlyLeft = true;
					expression = AddBlock(ref i, tokensArr);
				}
				if (expression is CurlyRightBracketExpression)
				{
					if (wasCurlyLeft)
					{
						wasCurlyLeft = false;
						continue;
					}
					else
						throw (new Exception("Missing left curly bracket."));
				}
				be.AddExpression(expression);
			}
			TreeViewItem[] tvi = new TreeViewItem[2];
			tvi[0] = ExpressionToTree(be, c);
			BlockExpression optimized = (BlockExpression)
				SemanticAnalizer.Optimize(new BlockExpression(be), c);
			tvi[1] = ExpressionToTree(optimized, c);
			optimized.Interpret(c);
			return tvi;
		}

		private IExpression AddBlock(ref int lexemNumber, string[][] tokensArr)
		{
			BlockExpression be = new BlockExpression();
			IExpression expression = null;
			for (++lexemNumber; lexemNumber < tokensArr.Length; ++lexemNumber)
			{
				expression = Parse(tokensArr[lexemNumber]);
				if (expression is ConditionalExpression ce)
					HandleConditional(ce, tokensArr, ref lexemNumber);
				if (expression is CurlyLeftBracketExpression)
				{
					expression = AddBlock(ref lexemNumber, tokensArr);
				}
				else if (expression is CurlyRightBracketExpression)
				{
					break;
				}
				be.AddExpression(expression);
			}
			if (lexemNumber == tokensArr.Length &&
				expression is CurlyRightBracketExpression == false)
				throw (new Exception("Can not find curly right bracket."));
			return be;
		}

		private void HandleConditional(ConditionalExpression ce, string[][] tokensArr, ref int i)
		{
			IExpression check = Parse(tokensArr[++i]);
			if (check is CurlyLeftBracketExpression == false)
				throw (new Exception("Conditional statement needs a block after it."));
			ce.positiveCase = AddBlock(ref i, tokensArr);
			if (ce is IfExpression ie)
				HandleElseExpression(ie, tokensArr, ref i);
		}

		private void HandleElseExpression(IfExpression ie, string[][] tokensArr, ref int i)
		{
			if (i + 1 >= tokensArr.Length)
				return;
			IExpression elseExpr = Parse(tokensArr[++i]);
			if (elseExpr is ElseExpression e)
			{
				IExpression check;
				check = Parse(tokensArr[++i]);
				if (check is CurlyLeftBracketExpression == false)
					throw (new Exception("'else' statement needs a block after it."));
				e.elseCase = AddBlock(ref i, tokensArr);
				ie.negativeCase = e;
			}
			else
			{
				--i;
			}
		}

		private IExpression Parse(string[] tokens)
		{
			List<IExpression> expressions = Parser.ParseTokens(tokens);
			ReversePolishNotation rpn = new ReversePolishNotation(expressions);
			return rpn.BuildTree();
		}

		private TreeViewItem ExpressionToTree(IExpression expression, Context c)
		{
			TreeViewItem tree = new TreeViewItem();
			TreeViewItem node = null;
			tree.Header = expression.GetType().Name;
			switch (expression)
			{
				case PropertyExpression pe:
					{
						tree.Header = $"({pe.AssignableName}.{pe.PropertyName}) {tree.Header}";
						break;
					}
				case AssignmentExpression ae:
					{
						tree.Items.Add(ExpressionToTree(ae.firstExpression, c));
						tree.Items.Add(ExpressionToTree(ae.secondExpression, c));
						break;
					}
				case DeleteExpression de:
					{
						tree.Items.Add(ExpressionToTree(de.firstExpression, c));
						break;
					}
				case NewExpression ne:
					{
						tree.Items.Add(ExpressionToTree(ne.firstExpression, c));
						tree.Items.Add(ExpressionToTree(ne.secondExpression, c));
						break;
					}
				case BlockExpression be:
					{
						List<IExpression> expressions = be.GetExpressions();
						foreach (var expr in expressions)
						{
							tree.Items.Add(ExpressionToTree(expr, c));
						}
						break;
					}
				case BinaryExpression bin:
					{
						tree.Items.Add(ExpressionToTree(bin.firstExpression, c));
						tree.Items.Add(ExpressionToTree(bin.secondExpression, c));
						break;
					}
				case UnaryExpression u:
					{
						tree.Items.Add(ExpressionToTree(u.firstExpression, c));
						break;
					}
				case IfExpression ie:
					{
						node = ExpressionToTree(ie.condition, c);
						node.Header = $"(Condtion) {node.Header}";
						tree.Items.Add(node);
						node = ExpressionToTree(ie.positiveCase, c);
						node.Header = $"(Positive case) {node.Header}";
						tree.Items.Add(node);
						if (ie.negativeCase != null)
						{
							node = ExpressionToTree(ie.negativeCase, c);
							node.Header = $"(Negative case) {node.Header}";
							tree.Items.Add(node);
						}
						break;
					}
				case ElseExpression ee:
					{
						tree.Items.Add(ExpressionToTree(ee.elseCase, c));
						break;
					}
				case WhileExpression we:
					{
						node = ExpressionToTree(we.condition, c);
						node.Header = $"(Condtion) {node.Header}";
						tree.Items.Add(node);
						node = ExpressionToTree(we.positiveCase, c);
						node.Header = $"(Positive case) {node.Header}";
						tree.Items.Add(node);
						break;
					}
				case ConstantExpression ce:
					{
						tree.Header = $"({ce.Interpret(null)}) {tree.Header}";
						break;
					}
				case VariableExpression ve:
					{
						tree.Header = $"({ve.Interpret(null)}) {tree.Header}";
						break;
					}
				case ObjectExpression oe:
					{
						tree.Header = $"({oe.AssignableName}) {tree.Header}";
						break;
					}
				case TypeExpression te:
					{
						tree.Header = $"({te.Interpret(null)}) {tree.Header}";
						break;
					}
				default:
					{
						break;
					}

			}
			return tree;
		}
	}
}
