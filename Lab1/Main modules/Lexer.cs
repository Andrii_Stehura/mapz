﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	static class Lexer
	{
		public static string[][] Tokenize(string lexems)
		{
			string[] lines = lexems.Split(Environment.NewLine);
			string[][] tokens = new string[lines.Length][];
			for (int i = 0; i < lines.Length; ++i)
			{
				lines[i] = lines[i].Trim();
				tokens[i] = lines[i].Split(' ');
			}
			return ClearSpaces(tokens);
		}

		private static string[][] ClearSpaces(string[][] tokens)
		{
			List<string[]> clearedLexems = new List<string[]>();
			List<string> clearedTokens = new List<string>();
			foreach (string[] strArr in tokens)
			{
				clearedTokens.Clear();
				string[] buf = strArr;
				if (strArr[0] == "{" || strArr[0] == "}")
				{
					buf = new string[1];
					buf[0] = strArr[0];
					clearedLexems.Add(buf);

					buf = new string[strArr.Length - 1];
					for (int i = 1; i < strArr.Length; ++i)
						buf[i - 1] = strArr[i];
				}
				foreach (string str in buf)
				{
					if (str.Contains(' ') || str == String.Empty)
						continue;
					clearedTokens.Add(str);
				}
				if (clearedTokens.Count > 0)
					clearedLexems.Add(clearedTokens.ToArray());
			}
			return clearedLexems.ToArray();
		}
	}
}
