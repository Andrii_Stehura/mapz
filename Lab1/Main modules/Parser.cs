﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	static class Parser
	{
		public static List<IExpression> ParseTokens(string[] tokens)
		{
			List<IExpression> expressions = new List<IExpression>();
			IExpression expr;
			for (int i = 0; i < tokens.Length; ++i)
			{
				switch (tokens[i].ToLower())
				{
					case "exclude":
						{
							expr = new ExcludeExpression();
							break;
						}
					case "xor":
						{
							expr = new XorExpression();
							break;
						}
					case "union":
						{
							expr = new UnionExpression();
							break;
						}
					case "intersect":
						{
							expr = new IntersectExpression();
							break;
						}
					case "clear":
						{
							expr = new ClearExpression();
							break;
						}
					case "draw":
						{
							expr = new DrawExpression();
							break;
						}
					case "==":
						{
							expr = new EqualsExpression();
							break;
						}
					case "<":
						{
							expr = new LowerExpression();
							break;
						}
					case ">":
						{
							expr = new GreaterExpression();
							break;
						}
					case "while":
						{
							expr = new WhileExpression();
							break;
						}
					case "=":
						{
							expr = new AssignmentExpression();
							break;
						}
					case "new":
						{
							if (i + 2 <= tokens.Length)
							{
								expr = new NewExpression();
								break;
							}
							throw (new Exception("Wrong 'new' syntax!"));
						}
					case "#":
						{
							if (i + 1 <= tokens.Length)
							{
								expr = new DeleteExpression();
								break;
							}
							throw (new Exception("Wrong '#' syntax!"));
						}
					case "+":
						{
							expr = new AddExpression();
							break;
						}
					case "-":
						{
							expr = new SubExpression();
							break;
						}
					case "*":
						{
							expr = new MultiplyExpression();
							break;
						}
					case "/":
						{
							expr = new DivisionExpression();
							break;
						}
					case "(":
						{
							expr = new LeftBracketExpression();
							break;
						}
					case ")":
						{
							expr = new RightBracketExpression();
							break;
						}
					case "{":
						{
							expr = new CurlyLeftBracketExpression();
							break;
						}
					case "}":
						{
							expr = new CurlyRightBracketExpression();
							break;
						}
					case "if":
						{
							expr = new IfExpression();
							break;
						}
					case "else":
						{
							expr = new ElseExpression();
							break;
						}
					case "and":
						{
							expr = new AndExpression();
							break;
						}
					case "or":
						{
							expr = new OrExpression();
							break;
						}
					case "not":
						{
							expr = new NotExpression();
							break;
						}
					case "circle":
					case "rectangle":
					case "triangle":
					case "variable":
						{
							expr = new TypeExpression(tokens[i]);
							break;
						}
					default:
						{
							if (Char.IsDigit(tokens[i][0]))
							{
								expr = new ConstantExpression(tokens[i]);
								break;
							}

							if (IsUppercase(tokens[i]))
							{
								expr = new ColorExpression(tokens[i]);
								break;
							}

							if (tokens[i].Contains('.'))
							{
								expr = new PropertyExpression(tokens[i]);
								break;
							}
							else
							{
								expr = new ObjectExpression(tokens[i]);
							}
							break;
						}
				}
				expressions.Add(expr);
			}
			return expressions;
		}

		private static bool IsUppercase(string str)
		{
			foreach (char ch in str)
			{
				if (!char.IsUpper(ch))
					return false;
			}
			return true;
		}
	}
}
