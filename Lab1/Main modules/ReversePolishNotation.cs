﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace FiguresInterpreter
{
	class ReversePolishNotation
	{
		List<IExpression> expressions;
		Stack<IExpression> operators;
		Stack<IExpression> output;
		public ReversePolishNotation(List<IExpression> expressions)
		{
			this.expressions = new List<IExpression>(expressions);
			operators = new Stack<IExpression>();
			output = new Stack<IExpression>();
		}

		public IExpression BuildTree()
		{
			IExpression result;
			foreach (var expr in expressions)
			{

				AddToStack(expr);
			}
			result = GetRoot();

			return result;
		}

		void AddToStack(IExpression expr) 
		{
			switch (expr)
			{
				case BracketExpression br:
					{
						switch (br)
						{
							case CurlyLeftBracketExpression clb:
							case CurlyRightBracketExpression crb:
							case LeftBracketExpression lb:
								{
									operators.Push(br);
									return;
								}
							case RightBracketExpression rb:
								{
									expr = HandleRightBracket();
									if (expr == null)
										return;
									else
									{
										output.Push(expr);
										break;
									}
								}
						}
						break;
					}
				case IPrioriable p:
					{
						if (operators.Count > 0)
						{
							if (p.GetPriority() <= ((IPrioriable)operators.Peek()).GetPriority())
							{
								IExpression exp = operators.Pop();
								if (!(exp is BracketExpression))
									output.Push(exp);
								else
									operators.Push(exp);
							}
						}
						operators.Push(expr);
						break;
					}
				default:
					{
						output.Push(expr);
						break;
					}
			}
		}

		IExpression HandleRightBracket()
		{
			if (operators.Count < 1)
				throw (new Exception("Can not find left bracket!"));

			IExpression ex;
			while ((ex = operators.Pop()).GetType()
				!= typeof(LeftBracketExpression))
			{
				output.Push(ex);
				if (operators.Count < 1)
					break;
			}

			if (ex.GetType() == typeof(LeftBracketExpression) && operators.Count > 1)
				return operators.Pop();

			return null;
		}

		IExpression GetRoot()
		{ 
			while(operators.Count > 0)
				output.Push(operators.Pop());
			if (output.Count == 0)
				throw (new Exception("Expressions stack is empty."));
			IExpression root = output.Pop();
			SetChildren(root);
			return root;
		}

		void SetChildren(IExpression node)
		{
			//try
			//{
				switch (node)
				{
					case ConditionalExpression ce:
						{
							ce.condition = output.Pop();
							SetChildren(ce.condition);
							return;
						}
					case BinaryExpression b:
						{
							b.secondExpression = output.Pop();
							SetChildren(b.secondExpression);
							b.firstExpression = output.Pop();
							SetChildren(b.firstExpression);
							return;
						}
					case UnaryExpression u:
						{
							u.firstExpression = output.Pop();
							SetChildren(u.firstExpression);
							return;
						}
					default:
						{
							return;
						}
				}
			//}
			//catch (OptimizationException oe)
			//{
			//	return;
			//}
		}
	}

	//class OptimizationException : Exception{ }
}
