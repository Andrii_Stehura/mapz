﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiguresInterpreter
{
	static class SemanticAnalizer
	{
		public static IExpression Optimize(IExpression expression, Context c)
		{
			switch (expression)
			{
				case BlockExpression be:
					{
						List<IExpression> expressions = be.GetExpressions();
						for (int i = 0; i < expressions.Count; ++i)
						{
							expressions[i] = Optimize(expressions[i], c);
						}
						expressions.RemoveAll((x) => x == null);
						return new BlockExpression(expressions);
					}
				case ConditionalExpression ce:
					{
						var res = ce.condition.Interpret(c);
						if (!(res is int))
							res = ((IExpression)res).Interpret(c);

						if ((int)res > 0)
						{
							expression = Optimize(ce.positiveCase, c);
						}
						else
						{
							if (!(ce is IfExpression ie))
								expression = null;
							else
							{
								if (ie.negativeCase == null)
									expression = null;
								else
								{
									ElseExpression ee = (ElseExpression)ie.negativeCase;
									expression = Optimize(ee.elseCase, c);
								}
							}
						}
						break;
					}
				case AssignmentExpression a:
					{
						if (a.firstExpression is NewExpression ne)
						{
							a.Interpret(c);
							expression = null;
						}
						break;
					}
				case NewExpression n:
					{
						n.Interpret(c);
						expression = null;  
						break;
					}
				case ObjectExpression o:
					{
						expression = null;
						break;
					}
			}
			return expression;
		}
	}
}
		
