﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FiguresInterpreter;
/*
new variable a = 5
new variable b = 6
new triangle t
t.startX = 50
t.startY = 50
t.side = 100
t.color = GREEN
if ( a > b )
{
new rectangle r
r.startX = 0
r.startY = 40
r.height = 10
r.width = 100
r.color = RED
draw r intersect t
}
else
{
new circle c
c.centerX = 60
c.centerY = 30
c.radius = 20
c.color = YELLOW
draw c intersect t
}
# a
# b
# c
# t
# r*/

namespace MAPZ_1
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		Context context;
		public MainWindow()
		{
			InitializeComponent();
			context = new Context();
		}

		private void button_Interpret_Click(object sender, RoutedEventArgs e)
		{
			if (textBox_input.Text.Length == 0)
				return;
			try
			{
				Interpreter i = new Interpreter(textBox_input.Text);
				treeView.Items.Clear();
				treeViewOpt.Items.Clear();
				context.drawingGroup.Children.Clear();
				TreeViewItem[] viewItems = i.Interpret(context);
				treeView.Items.Add(viewItems[0]);
				treeViewOpt.Items.Add(viewItems[1]);
				DrawingImage di = new DrawingImage();
				di.Drawing = context.drawingGroup;
				outputImg.Source = di;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Error!",
					MessageBoxButton.OK, MessageBoxImage.Error);
			}

			textBox_objects.Clear();
			string[] objects = context.GetObjectsToString();
			if (objects.Length < 1)
				return;
			foreach (string str in objects)
			{
				textBox_objects.Text += str + Environment.NewLine + Environment.NewLine;
			}
		}
	}
}
