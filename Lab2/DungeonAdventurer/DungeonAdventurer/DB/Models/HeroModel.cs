﻿using System;
using System.Collections.Generic;
using System.Text;
using DungeonAdventurer.Entities.Hero_Entities;
using DungeonAdventurer.Entities.Item_Entities.Equipment;

namespace DungeonAdventurer.DB.Models
{
	class HeroModel: ICloneable
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public HeroRace Race { get; set; }
		public HeroClass Class { get; set; }
		public int Level { get; set; }
		public int Experience { get; set; }
		//public ICollection<EquipmentModel> Equipments { get; set; }
		public ICollection<PointsModel> Points { get; set; }
		public ICollection<ParametersModel> Parameters { get; set; }

		public HeroModel() { }
		public HeroModel(HeroModel h)
		{
			Name = h.Name;
			Race = h.Race;
			Class = h.Class;
			Level = h.Level;
			Experience = h.Experience;
		}

		public object Clone()
		{
			return new HeroModel(this);
		}
	}
}
