﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.DB.Models
{
	class ParametersModel: ICloneable
	{
		public int Id { get; set; }
		public int Strength { get; set; }
		public int Intelligence { get; set; }
		public int Agility { get; set; }

		public HeroModel Hero { get; set; }

		public ParametersModel(){}

		public ParametersModel(ParametersModel pm)
		{
			this.Strength = pm.Strength;
			this.Intelligence = pm.Intelligence;
			this.Agility = pm.Agility;
		}

		public object Clone()
		{
			return new ParametersModel(this);
		}
	}
}
