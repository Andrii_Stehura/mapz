﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.DB.Models
{
	class PointsModel: ICloneable
	{
		public int Id { get; set; }
		public int Health { get; set; }
		public int Armor { get; set; }
		public int Mana { get; set; }

		public HeroModel Hero { get; set; }

		public PointsModel(){}
		public PointsModel(PointsModel pointsModel)
		{
			this.Health = pointsModel.Health;
			this.Armor = pointsModel.Armor;
			this.Mana = pointsModel.Mana;
		}

		public object Clone()
		{
			return new PointsModel(this);
		}
	}
}
