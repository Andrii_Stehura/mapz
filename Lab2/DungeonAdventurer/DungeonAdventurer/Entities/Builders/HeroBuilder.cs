﻿using System;
using System.Collections.Generic;
using System.Text;
using DungeonAdventurer.DB;
using DungeonAdventurer.DB.Models;
using DungeonAdventurer.Entities.Hero_Entities;
using System.Linq;
using DungeonAdventurer.Views;

namespace DungeonAdventurer.Entities.Builders
{
	class HeroBuilder
	{
		public Hero BuildHero(HeroModel h, List<ParametersModel> pm, List<PointsModel> pom)
		{
			return new Hero(h, pom.Single((x) => x.Hero.Id == h.Id),
				pm.Single((x) => x.Hero.Id == h.Id));
		}
	}
}
