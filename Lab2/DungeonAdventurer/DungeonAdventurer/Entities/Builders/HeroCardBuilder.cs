﻿using DungeonAdventurer.DB;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DungeonAdventurer.DB.Models;
using DungeonAdventurer.Entities.Hero_Entities;
using DungeonAdventurer.Views;

namespace DungeonAdventurer.Entities.Builders
{
	class HeroCardBuilder: HeroBuilder
	{
		public HeroCard BuildHeroCard(Hero h)
		{
			HeroCard hc = new HeroCard();
			hc.DataContext = h;
			return hc;
		}
	}
}
