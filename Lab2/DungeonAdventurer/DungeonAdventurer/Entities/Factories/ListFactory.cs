﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Factories
{
	abstract class ListFactory<T>
	{
		public abstract List<T> MakeList(DbContext db);
	}
}
