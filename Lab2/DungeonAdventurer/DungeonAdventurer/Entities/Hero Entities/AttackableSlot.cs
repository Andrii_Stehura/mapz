﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Hero_Entities
{
	public enum AttackableSlot
	{
		Head, Body, Legs
	}
}
