﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Hero_Entities
{
	public class Parameters: ICloneable
	{
		//properties
		public int Strength { get; set; }
		public int Intelligence { get; set; }
		public int Agility { get; set; }

		//methods
		public Parameters()
		{
			Strength = 0; Intelligence = 0; Agility = 0;
		}

		public Parameters(int intelligence, int strength, int agility)
		{
			Strength = strength; 
			Intelligence = Intelligence; 
			Agility = agility;
		}

		public Parameters(Parameters p)
		{
			Intelligence = p.Intelligence;
			Strength = p.Strength;
			Agility = p.Agility;
		}
		public object Clone()
		{
			return new Parameters(this);
		}
	}
}
