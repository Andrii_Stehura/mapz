﻿using System;
using System.Collections.Generic;
using System.Text;
using DungeonAdventurer.Entities.Hero_Entities;

namespace DungeonAdventurer.Entities.Item_Entities.Equipment
{
	class Equipment
	{
		//variables
		private List<Modifiable> Modifiers;

		//properties
		public EquipmentSlot Slot { get; set; }
		public string Name { get; set; }
		public int RequiredLevel { get; set; }

		public EquipmentRarity Rarity { get; private set; }

		//methods
		public Equipment()
		{
			Modifiers = new List<Modifiable>();
		}

		public void Modify(Hero hero)
		{
			foreach (var modifier in Modifiers)
				modifier.Modify(hero);
		}

		public void Demodify(Hero hero)
		{
			foreach (var modifier in Modifiers)
				modifier.Demodify(hero);
		}
	}
}
