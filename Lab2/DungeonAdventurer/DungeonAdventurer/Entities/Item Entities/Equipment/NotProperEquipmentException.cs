﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Item_Entities.Equipment
{
	class NotProperEquipmentException : Exception
	{
		public new string Message { get; private set; }
		public NotProperEquipmentException(EquipmentSlot slot, Equipment wrongItem)
		{
			switch (slot)
			{
				case EquipmentSlot.Head:
					{
						Message = $"Can not put {wrongItem.Name} on hero's head!";
						break;
					}
				case EquipmentSlot.Body:
					{
						Message = $"Can not put {wrongItem.Name} on hero's body!";
						break;
					}
				case EquipmentSlot.Legs:
					{
						Message = $"Can not put {wrongItem.Name} on hero's legs!";
						break;
					}
				case EquipmentSlot.Weapon:
					{
						Message = $"Can not put {wrongItem.Name} in hero's arms!";
						break;
					}
			}
		}
	}
}
