﻿using System;
using System.Collections.Generic;
using System.Text;
using DungeonAdventurer.Entities.Hero_Entities;
using DungeonAdventurer.DB;
using DungeonAdventurer.DB.Models;

namespace DungeonAdventurer.ViewModels
{
	class CreateHeroViewModel: BaseViewModel
	{
		HeroModel hm;
		PointsModel pom;
		ParametersModel par;
		int pointsLeft = 3;

		public CreateHeroViewModel()
		{
			hm = new HeroModel();
			par = new ParametersModel();
			pom = new PointsModel();

			hm.Level = 1;
			pom.Armor = 10;
			pom.Health = 10;
			pom.Mana = 10;

			par.Agility = 1;
			par.Intelligence = 1;
			par.Strength = 1;
		}

		public int PointsLeft
		{
			get { return pointsLeft; }
			set 
			{
				if (value == pointsLeft)
					return;
				pointsLeft = value;
				OnPropertyChanged();
			}  
		}

		public string Name 
		{
			get { return hm.Name; }
			set
			{
				if (hm.Name != value)
				{
					hm.Name = value;
					OnPropertyChanged();
				}
			}
		}

		public string Race
		{
			get 
			{
				switch (hm.Race)
				{
					case HeroRace.Elf:
						return "Elf";
					case HeroRace.Orc:
						return "Orc";
					case HeroRace.Human:
						return "Human";
					default:
						return "";
				}
			}
			set
			{
				switch (value)
				{
					case "Elf":
						{
							hm.Race = HeroRace.Elf;
							break;
						}
					case "Orc":
						{
							hm.Race = HeroRace.Orc;
							break;
						}
					case "Human":
						{
							hm.Race = HeroRace.Human;
							break;
						}
				}
				OnPropertyChanged();
			}
		}

		public string Class
		{
			get
			{
				switch (hm.Class)
				{
					case HeroClass.Fighter:
						return "Fighter";
					case HeroClass.Mage:
						return "Mage";
					case HeroClass.Rogue:
						return "Rogue";
					default:
						return "";
				}
			}
			set
			{
				switch (value)
				{
					case "Rogue":
						{
							hm.Class = HeroClass.Rogue;
							break;
						}
					case "Mage":
						{
							hm.Class = HeroClass.Mage;
							break;
						}
					case "Fighter":
						{
							hm.Class = HeroClass.Fighter;
							break;
						}
				}
				OnPropertyChanged();
			}
		}

		public int Level
		{
			get
			{
				return hm.Level;
			}
		}

		public int Agility
		{
			get
			{
				return par.Agility;
			}
			set
			{
				if (par.Agility != value)
				{
					par.Agility = value;
					OnPropertyChanged();
				}
			}
		}

		public int Intelligence
		{
			get
			{
				return par.Intelligence;
			}
			set
			{
				if (par.Intelligence != value)
				{
					par.Intelligence = value;
					OnPropertyChanged();
				}
			}
		}

		public int Strength
		{
			get
			{
				return par.Strength;
			}
			set
			{
				if (par.Strength != value)
				{
					par.Strength = value;
					OnPropertyChanged();
				}
			}
		}

		public int Armor
		{
			get
			{
				return pom.Armor;
			}
			set
			{
				if (pom.Armor != value)
				{
					pom.Armor = value;
					OnPropertyChanged();
				}
			}
		}

		public int Health
		{
			get
			{
				return pom.Health;
			}
			set
			{
				if (pom.Health != value)
				{
					pom.Health = value;
					OnPropertyChanged();
				}
			}
		}

		public int Mana
		{
			get
			{
				return pom.Mana;
			}
			set
			{
				if (pom.Mana != value)
				{
					pom.Mana = value;
					OnPropertyChanged();
				}
			}
		}

		public DelegateCommand HeroesClick
		{
			get
			{
				return new DelegateCommand((obj) => ViewsRouter.Instance.RouteTo(Routes.Heroes));
			}
		}

		private string IntToClassString(int i)
		{
			switch (i)
			{
				case 0:
					{
						return "Fighter";
					}
				case 1:
					{
						return "Mage";
					}
				case 2:
					{
						return "Rogue";
					}
			}
			return "";
		}

		public object DataContextForHeroCard { get { return this; } }

		public DelegateCommand ClassChooseLower
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					int i = (int)hm.Class;
					if (i <= 0)
						return;
					Class = IntToClassString(--i);
				});
			}
		}

		public DelegateCommand ClassChooseHigher
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					int i = (int)hm.Class;
					if (i >= 2)
						return;
					Class = IntToClassString(++i);
				});
			}
		}

		public DelegateCommand RaceChooseLower
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					int i = (int)hm.Race;
					if (i <= 0)
						return;
					Race = IntToRaceString(--i);
				});
			}
		}

		public DelegateCommand RaceChooseHigher
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					int i = (int)hm.Race;
					if (i >= 2)
						return;
					Race = IntToRaceString(++i);
				});
			}
		}

		private string IntToRaceString(int v)
		{
			switch (v)
			{
				case 0:
					{
						return "Orc";
					}
				case 1:
					{
						return "Human";
					}
				case 2:
					{
						return "Elf";
					}
			}
			return "";
		}

		public DelegateCommand AgilityLower
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					if (pointsLeft >= 3 || Agility <= 1)
						return;

					++PointsLeft;
					--Agility;
				});
			}
		}
		public DelegateCommand AgilityHigher
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					if (pointsLeft < 1)
						return;

					--PointsLeft;
					++Agility;
				});
			}
		}

		public DelegateCommand IntelligenceLower
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					if (pointsLeft >= 3 || Intelligence <= 1)
						return;

					++PointsLeft;
					--Intelligence;
				});
			}
		}
		public DelegateCommand IntelligenceHigher
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					if (pointsLeft < 1)
						return;

					--PointsLeft;
					++Intelligence;
				});
			}
		}

		public DelegateCommand StrengthLower
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					if (pointsLeft >= 3 || Strength <= 1)
						return;

					++PointsLeft;
					--Strength;
				});
			}
		}
		public DelegateCommand StrengthHigher
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					if (pointsLeft < 1)
						return;

					--PointsLeft;
					++Strength;
				});
			}
		}

		public DelegateCommand SaveHero
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					DA_DbContext db = new DA_DbContext();
					par.Hero = hm;
					pom.Hero = hm;
					db.Add(hm);
					db.Add(par);
					db.Add(pom);
					db.SaveChanges();
					ViewsRouter.Instance.RouteTo(Routes.Heroes);
				});
			}

		}
	}
}
