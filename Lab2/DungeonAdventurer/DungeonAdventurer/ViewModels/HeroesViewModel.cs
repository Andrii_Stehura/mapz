﻿using DungeonAdventurer.Entities.Hero_Entities;
using DungeonAdventurer.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using DungeonAdventurer.Entities.Factories;
using DungeonAdventurer.DB;
using System.Windows.Input;

namespace DungeonAdventurer.ViewModels
{
	class HeroesViewModel: BaseViewModel
	{
		List<HeroCard> heroesCardsList;

		public HeroesViewModel()
		{
			App.Current.MainWindow.Cursor = Cursors.Wait;
			ListFactory<HeroCard> hclf = new HeroCardsListFactory();
			heroesCardsList = hclf.MakeList(new DA_DbContext());
			App.Current.MainWindow.Cursor = Cursors.Arrow;
		}

		public List<HeroCard> HeroesCardsList 
		{
			get
			{
				return heroesCardsList;
			} 
		}

		public DelegateCommand MenuClick
		{
			get { return new DelegateCommand((obj) => {
				ViewsRouter.Instance.RouteTo(Routes.MainMenu);
				}); }
		}

		public DelegateCommand CreateClick
		{
			get 
			{
				return new DelegateCommand((obj) =>
				{
					ViewsRouter.Instance.RouteTo(Routes.CreateHero);
				});
			}

		}
	}
}
