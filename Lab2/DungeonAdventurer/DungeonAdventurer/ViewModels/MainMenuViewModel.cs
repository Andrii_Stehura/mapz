﻿using DungeonAdventurer.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace DungeonAdventurer.ViewModels
{
	class MainMenuViewModel: BaseViewModel
	{
		public DelegateCommand PlayClick
		{
			get
			{
				return new DelegateCommand((obj) => { MessageBox.Show("Game"); });
			}
		}

		public DelegateCommand HeroesClick
		{
			get
			{
				return new DelegateCommand((obj) => ViewsRouter.Instance.RouteTo(Routes.Heroes));
			}
		}

		public DelegateCommand ExitClick
		{
			get
			{
				return new DelegateCommand((obj) => App.Current.MainWindow.Close());
			}
		}
	}
}
