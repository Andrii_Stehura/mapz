﻿using DungeonAdventurer.DB.Models;
using DungeonAdventurer.Entities.Hero_Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonAdventurer.DB.Adapters
{
	class HeroAdapter: DbAdapter
	{
		public HeroAdapter(DbContext dc):base(dc){ }

		public override void Save(object obj)
		{
			Hero h = obj as Hero;
			if (h == null)
				throw (new Exception($"Can not save object of type {obj.GetType().Name}"
					+ " as Hero."));
			
			ParametersModel pm;
			PointsModel ptm;
			HeroModel hm = db.Heroes.Find(h.ID);
			if (hm == null)
			{
				hm = new HeroModel(h);
				pm = new ParametersModel(h);
				pm.Hero = hm;
				ptm = new PointsModel(h);
				ptm.Hero = hm;
			}
			else
			{
				hm.Class = h.Class;
				hm.Experience = h.Experience;
				hm.Level = h.Level;
				hm.Name = h.Name;
				hm.Race = h.Race;

				pm = db.Parameters.Single((x) => x.Hero.Id == h.ID);
				pm.Agility = h.Agility;
				pm.Strength = h.Strength;
				pm.Intelligence = h.Intelligence;

				ptm = db.Points.Single((x) => x.Hero.Id == h.ID);
				ptm.Armor = h.Armor;
				ptm.Health = h.Health;
				ptm.Mana = h.Mana;
			}

			db.Add(hm);
			db.Add(pm);
			db.Add(ptm);
			db.SaveChanges();
		}
		public override void Remove(object obj)
		{
			Hero h = obj as Hero;
			if (h == null)
				throw (new Exception($"Can not remove object of type {obj.GetType().Name}"
					+ " as Hero."));

			HeroModel hm = db.Heroes.Find(h.ID);
			if (hm == null)
				return;

			ParametersModel pm = db.Parameters.Single((x) => x.Hero.Id == h.ID);
			PointsModel ptm = db.Points.Single((x) => x.Hero.Id == h.ID);

			db.Parameters.Remove(pm);
			db.Points.Remove(ptm);
			db.Heroes.Remove(hm);
			db.SaveChanges();
		}
	}
}
