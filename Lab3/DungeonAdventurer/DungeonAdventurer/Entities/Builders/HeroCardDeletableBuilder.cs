﻿using DungeonAdventurer.Entities.Hero_Entities;
using DungeonAdventurer.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Builders
{
	class HeroCardDeletableBuilder: HeroBuilder
	{
		public HeroCardDeletable BuildHeroCard(Hero h)
		{
			HeroCardDeletable hd = new HeroCardDeletable();
			hd.DataContext = h;
			return hd;
		}
	}
}
