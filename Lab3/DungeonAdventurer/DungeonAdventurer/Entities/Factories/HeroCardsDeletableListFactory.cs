﻿using DungeonAdventurer.DB;
using DungeonAdventurer.DB.Models;
using DungeonAdventurer.Entities.Builders;
using DungeonAdventurer.Entities.Hero_Entities;
using DungeonAdventurer.Views;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonAdventurer.Entities.Factories
{
	class HeroCardsDeletableListFactory: ListFactory<HeroCardDeletable>
	{
		public override List<HeroCardDeletable> MakeList(DbContext db)
		{
			List<HeroCardDeletable> heroCards = new List<HeroCardDeletable>();
			HeroCardDeletableBuilder hcb = new HeroCardDeletableBuilder();
			DA_DbContext context = db as DA_DbContext;
			List<HeroModel> heroes = context.Heroes.ToList();
			List<ParametersModel> parameters = context.Parameters.ToList();
			List<PointsModel> points = context.Points.ToList();
			foreach (var x in heroes)
			{
				Hero h = hcb.BuildHeroProxy(x, parameters, points);
				heroCards.Add(hcb.BuildHeroCard(h));
			}
			return heroCards;
		}
	}
}
