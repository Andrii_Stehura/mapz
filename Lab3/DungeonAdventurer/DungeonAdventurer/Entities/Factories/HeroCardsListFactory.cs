﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DungeonAdventurer.Entities.Builders;
using DungeonAdventurer.DB;
using DungeonAdventurer.DB.Models;
using DungeonAdventurer.Entities.Hero_Entities;
using DungeonAdventurer.Views;

namespace DungeonAdventurer.Entities.Factories
{
	class HeroCardsListFactory : ListFactory<HeroCard>
	{
		public override List<HeroCard> MakeList(DbContext db)
		{
			List<HeroCard> heroCards = new List<HeroCard>();
			HeroCardBuilder hcb = new HeroCardBuilder();
			DA_DbContext context = db as DA_DbContext;
			List<HeroModel> heroes = context.Heroes.ToList();
			List<ParametersModel> parameters = context.Parameters.ToList();
			List<PointsModel> points = context.Points.ToList();
			foreach (var x in heroes)
			{
				Hero h = hcb.BuildHeroProxy(x, parameters, points);
				heroCards.Add(hcb.BuildHeroCard(h));
			}
			return heroCards;
		}
	}
}
