﻿using System;
using System.Collections.Generic;
using DungeonAdventurer.DB.Models;
using DungeonAdventurer.Entities.Item_Entities.Equipment;

namespace DungeonAdventurer.Entities.Hero_Entities
{
	class Hero
	{
		private HeroModel heroModel;
		private PointsModel pointsModel;
		private ParametersModel parModel;

		public int Health
		{
			get { return pointsModel.Health; }
			set { pointsModel.Health = value; }
		}

		public int Armor
		{
			get { return pointsModel.Armor; }
			set { pointsModel.Armor = value; }
		}

		public int Mana
		{
			get { return pointsModel.Mana; }
			set { pointsModel.Mana = value; }
		}

		public int Strength
		{
			get { return parModel.Strength; }
			set { parModel.Strength = value; }
		}

		public int Intelligence
		{
			get { return parModel.Intelligence; }
			set { parModel.Intelligence = value; }
		}

		public int Agility
		{
			get { return parModel.Agility; }
			set { parModel.Agility = value; }
		}

		public HeroClass Class
		{
			get { return heroModel.Class; }
		}

		public HeroRace Race
		{
			get { return heroModel.Race; }
		}

		public string Name
		{
			get { return heroModel.Name; }
		}

		public int Experience
		{
			get { return heroModel.Experience; }
		}

		public int Level
		{
			get { return heroModel.Level; }
		}

		public int ID
		{
			get { return (heroModel.Id != 0) ? heroModel.Id : -1; }
		}

		public Hero(HeroModel h, PointsModel pts, ParametersModel par)
		{
			heroModel = h;
			pointsModel = pts;
			parModel = par;
		}
	}
}