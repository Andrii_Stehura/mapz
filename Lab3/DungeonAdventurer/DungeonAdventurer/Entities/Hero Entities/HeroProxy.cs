﻿using DungeonAdventurer.DB.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Hero_Entities
{
	class HeroProxy : Hero
	{
		public static int GetMana(HeroModel h, PointsModel pts, ParametersModel par)
		{
			return (int)(pts.Mana + ProxyMultipliers.GetMultiplier(
					Parameter.Intelligence, h.Class, h.Race) * par.Intelligence);
		}
		public static int GetHealth(HeroModel h, PointsModel pts, ParametersModel par)
		{
			return (int)(pts.Health + ProxyMultipliers.GetMultiplier(
					Parameter.Strength, h.Class, h.Race) * par.Strength);
		}
		public static int GetArmor(HeroModel h, PointsModel pts, ParametersModel par)
		{
			return (int)(pts.Armor + ProxyMultipliers.GetMultiplier(
					Parameter.Agility, h.Class, h.Race) * par.Agility);
		}
		public HeroProxy(HeroModel h, PointsModel pts, ParametersModel par) : base(h, pts, par)
		{
			Health = GetHealth(h, pts, par);
			Armor = GetArmor(h, pts, par);
			Mana = GetMana(h, pts, par);
		}
	}

	static class ProxyMultipliers
	{
		public const double MUL_LOW = 1;
		public const double MUL_NORM = 1.5;
		public const double MUL_HIGH = 2;

		public static double GetMultiplier(Parameter p, HeroClass c, HeroRace r)
		{
			switch (p)
			{
				case Parameter.Strength:
					return GetStrMultiplier(c, r);
				case Parameter.Intelligence:
					return GetIntMultiplier(c, r);
				case Parameter.Agility:
					return GetAgiMultiplier(c, r);
			}
			throw new Exception("Not proper parameter.");
		}

		public static double GetStrMultiplier(HeroClass c, HeroRace r)
		{
			double res = 1;
			switch (r)
			{
				case HeroRace.Elf:
					{
						res += MUL_LOW;
						break;
					}
				case HeroRace.Human:
					{
						res += MUL_NORM;
						break;
					}
				case HeroRace.Orc:
					{
						res += MUL_HIGH;
						break;
					}
			}
			switch (c)
			{
				case HeroClass.Mage:
					{
						res += MUL_LOW;
						break;
					}
				case HeroClass.Rogue:
					{
						res += MUL_NORM;
						break;
					}
				case HeroClass.Fighter:
					{
						res += MUL_HIGH;
						break;
					}
			}
			return res;
		}

		public static double GetIntMultiplier(HeroClass c, HeroRace r)
		{
			double res = 1;
			switch (r)
			{
				case HeroRace.Orc:
					{
						res += MUL_LOW;
						break;
					}
				case HeroRace.Human:
					{
						res += MUL_NORM;
						break;
					}
				case HeroRace.Elf:
					{
						res += MUL_HIGH;
						break;
					}
			}
			switch (c)
			{
				case HeroClass.Fighter:
					{
						res += MUL_LOW;
						break;
					}
				case HeroClass.Rogue:
					{
						res += MUL_NORM;
						break;
					}
				case HeroClass.Mage:
					{
						res += MUL_HIGH;
						break;
					}
			}
			return res;
		}

		public static double GetAgiMultiplier(HeroClass c, HeroRace r)
		{
			double res = 1;
			switch (r)
			{
				case HeroRace.Orc:
					{
						res += MUL_LOW;
						break;
					}
				case HeroRace.Human:
					{
						res += MUL_NORM;
						break;
					}
				case HeroRace.Elf:
					{
						res += MUL_HIGH;
						break;
					}
			}
			switch (c)
			{
				case HeroClass.Mage:
					{
						res += MUL_LOW;
						break;
					}
				case HeroClass.Fighter:
					{
						res += MUL_NORM;
						break;
					}
				case HeroClass.Rogue:
					{
						res += MUL_HIGH;
						break;
					}
			}
			return res;
		}
	}
}
