﻿using DungeonAdventurer.Entities.Hero_Entities;
using DungeonAdventurer.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using DungeonAdventurer.Entities.Factories;
using DungeonAdventurer.DB;
using System.Windows.Input;
using System.Windows;
using System.Linq;
using DungeonAdventurer.DB.Adapters;

namespace DungeonAdventurer.ViewModels
{
	class HeroesViewModel: BaseViewModel
	{
		List<HeroProxy> heroesList;

		public HeroesViewModel()
		{
			App.Current.MainWindow.Cursor = Cursors.Wait;
			ListFactory<HeroProxy> hclf = new HeroProxyListFactory();
			heroesList = hclf.MakeList(new DA_DbContext());
			App.Current.MainWindow.Cursor = Cursors.Arrow;
		}

		public List<HeroProxy> HeroesList 
		{
			get
			{
				return heroesList;
			} 
		}

		public DelegateCommand MenuClick
		{
			get { return new DelegateCommand((obj) => {
				ViewsRouter.Instance.RouteTo(Routes.MainMenu);
				}); }
		}

		public DelegateCommand CreateClick
		{
			get 
			{
				return new DelegateCommand((obj) =>
				{
					ViewsRouter.Instance.RouteTo(Routes.CreateHero);
				});
			}

		}

		public DelegateCommand DeleteClick
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					int id = (int)obj;
					HeroProxy h = heroesList.Single((x) => x.ID == id);
					MessageBoxResult res = MessageBox.Show($"Do you want delete" +
						$" {h.Name} " + "permanently?", "Attention!", MessageBoxButton.YesNo,
						MessageBoxImage.Warning, MessageBoxResult.No);
					
					if (res == MessageBoxResult.Yes)
					{
						DbAdapter dbAdapter = new HeroAdapter(new DA_DbContext());
						dbAdapter.Remove(h);
						heroesList.Remove(h);
						ViewsRouter.Instance.RouteTo(Routes.Heroes);
					}
				});
			}
		}
	}
}
