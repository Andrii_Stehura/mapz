﻿using DungeonAdventurer.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace DungeonAdventurer.ViewModels
{
	class ViewsRouter
	{
		private ViewsRouter() { }

		private static ViewsRouter instance;
		private static MainViewModel mainVM;

		private UserControl menu;
		private UserControl heroes;
		private UserControl createHero;

		public MainViewModel MainVM { get { return mainVM; } set { mainVM = value; } }

		static public ViewsRouter Instance
		{
			get
			{
				return instance ?? new ViewsRouter();
			}
		}

		public void RouteTo(Routes route)
		{
			switch (route)
			{
				case (Routes.MainMenu):
					{
						mainVM.CurrentPage = menu ?? (menu = new MainMenu());
						break;
					}
				case (Routes.Play):
					{
						//mainVM.CurrentPage = menu ?? new MainMenu();
						break;
					}
				case (Routes.Heroes):
					{
						mainVM.CurrentPage = heroes ?? (heroes = new HeroesView());
						break;
					}
				case (Routes.CreateHero):
					{
						mainVM.CurrentPage = createHero ?? (createHero = new CreateHero());
						break;
					}
			}
		}
	}
}
