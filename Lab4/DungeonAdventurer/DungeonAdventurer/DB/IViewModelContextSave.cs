﻿using DungeonAdventurer.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.DB
{
	interface IViewModelContextSave
	{
		public ViewModelContext Context { get; }
	}
}
