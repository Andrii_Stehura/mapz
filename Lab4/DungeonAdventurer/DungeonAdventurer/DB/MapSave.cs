﻿using DungeonAdventurer.Entities.Hero_Entities;
using DungeonAdventurer.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.DB
{
	[Serializable]
	class MapSave: IViewModelSave, IViewModelContextSave
	{
		private HeroProxy hero;
		private IUpdateProperties mapContext;
		private DateTime date;
		private ViewModelContext context;

		public MapSave(MapLevelViewModel maplevel)
		{
			hero = maplevel.Hero;
			mapContext = maplevel.MapContext;
			date = DateTime.Now;
			context = ViewsRouter.Instance.Context;
		}

		public DateTime Date { get { return date; } }
		public HeroProxy Hero { get { return hero; } }
		public IUpdateProperties MapContext { get { return mapContext; } }
		public ViewModelContext Context { get { return context; } }
		public string Name { get { return hero.Name; } }
	}
}
