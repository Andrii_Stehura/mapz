﻿using System;
using System.Collections.Generic;
using System.Text;
using DungeonAdventurer.Entities.Item_Entities;
using DungeonAdventurer.Entities.Item_Entities.Equipment;

namespace DungeonAdventurer.DB.Models
{
	class EquipmentModel
	{
		public int Id { get; set; }
		public EquipmentSlot Slot { get; set; }
		public string Name { get; set; }
		public int RequiredLevel { get; set; }
		public EquipmentRarity Rarity { get; private set; }

		public HeroModel Hero { get; set; }
	}
}
