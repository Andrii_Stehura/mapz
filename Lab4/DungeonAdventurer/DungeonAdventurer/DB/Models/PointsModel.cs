﻿using DungeonAdventurer.Entities.Hero_Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.DB.Models
{
	[Serializable]
	class PointsModel: ICloneable
	{
		public int Id { get; set; }
		public int Health { get; set; }
		public int Armor { get; set; }
		public int Mana { get; set; }

		public HeroModel Hero { get; set; }

		public PointsModel()
		{
			Health = 10;
			Armor = 10;
			Mana = 10;
		}
		public PointsModel(PointsModel pointsModel)
		{
			this.Health = pointsModel.Health;
			this.Armor = pointsModel.Armor;
			this.Mana = pointsModel.Mana;
		}

		public PointsModel(Hero h)
		{
			Health = h.Health;
			Armor = h.Armor;
			Mana = h.Mana;
		}

		public object Clone()
		{
			return new PointsModel(this);
		}
	}
}
