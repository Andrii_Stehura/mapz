﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace DungeonAdventurer.DB
{
	class SaveSerializer
	{
		private const string PATH = @"../../../../";

		public static bool CheckName(string name)
		{
			string fileName = $"{PATH}{name}.bin";
			return File.Exists(fileName);
		}

		public void SerializeAndSave(IViewModelSave save)
		{
			using (var stream = new MemoryStream())
			{
				try
				{
					new BinaryFormatter().Serialize(stream, save);
				}
				catch(Exception e)
				{
					MessageBox.Show($"Can not save because of exception:'{e.Message}'",
						"Error!", MessageBoxButton.OK, MessageBoxImage.Error);
					return;
				}
				string fileName = $"{PATH}{save.Name}.bin";
				Stream SaveFileStream = File.Create(fileName);
				BinaryFormatter serializer = new BinaryFormatter();
				serializer.Serialize(SaveFileStream, save);
				SaveFileStream.Close();
				MessageBox.Show("Saved!", "Success!", MessageBoxButton.OK, 
					MessageBoxImage.Information);
			}
		}

		public IViewModelSave Deserialize(string name)
		{
			IViewModelSave restored = null;
			try
			{
				string fileName = $"{PATH}{name}.bin";
				if (File.Exists(fileName))
				{
					using (Stream openFileStream = File.OpenRead(fileName))
					{
						BinaryFormatter deserializer = new BinaryFormatter();
						restored = deserializer.Deserialize(openFileStream)
							as IViewModelSave;
						openFileStream.Close();
					}
				}
			}
			catch(Exception e)
			{
				MessageBox.Show($"Can not open save beacause:'{e.Message}'.",
					"Error!", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			return restored;
		}
	}
}
