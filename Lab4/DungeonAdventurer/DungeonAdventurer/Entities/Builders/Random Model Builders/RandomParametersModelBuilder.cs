﻿using DungeonAdventurer.DB.Models;
using DungeonAdventurer.Entities.Builders.RandomModelBuilders;
using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Builders.Random_Model_Builders
{
	class RandomParametersModelBuilder : IRandomModelBuilder
	{
		private const double LOW_CHANCE = 0.3;
		private const double MED_CHANCE = 0.6;
		//else is high chance

		private const int PAR_POINTS = 3;
		private int lvl;

		public RandomParametersModelBuilder(int level)
		{
			lvl = level;
		}

		public object BuildModel()
		{
			ParametersModel p = new ParametersModel();
			int totalPoints = lvl * PAR_POINTS;
			while (totalPoints > 0)
			{
				Random r = new Random();
				double chance = r.NextDouble();
				if (chance <= LOW_CHANCE)
					p.Strength += lvl;
				else if (chance <= MED_CHANCE)
					p.Agility += lvl;
				else
					p.Intelligence += lvl;

				totalPoints -= lvl;
			}
			return p;
		}
	}
}
