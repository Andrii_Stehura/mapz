﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Factories
{
	abstract class ArrayFactory<T>
	{
		public abstract T[] MakeArray(int columns);
		public abstract T[][] MakeArray(int rows, int Columns);
	}
}
