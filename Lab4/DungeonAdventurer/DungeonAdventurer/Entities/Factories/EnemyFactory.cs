﻿using DungeonAdventurer.Entities.Builders;
using DungeonAdventurer.Entities.Hero_Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Factories
{
	class EnemyFactory : IHeroFactory
	{
		private const double LOW_CHANCE = 0.3;
		private const double MED_CHANCE = 0.6;
		//else is high chance

		int lvl;

		public EnemyFactory(int level)
		{
			lvl = level;
		}

		public Hero CreateHero()
		{
			Random rand = new Random();
			double chance = rand.NextDouble();
			if (chance <= LOW_CHANCE)
				++lvl;
			else if (chance > MED_CHANCE)
				--lvl;
			HeroBuilder hb = new HeroBuilder();
			return hb.BuildHeroProxy(lvl);
		}
	}
}
