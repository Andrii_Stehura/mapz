﻿using DungeonAdventurer.DB;
using DungeonAdventurer.DB.Models;
using DungeonAdventurer.Entities.Builders;
using DungeonAdventurer.Entities.Hero_Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonAdventurer.Entities.Factories
{
	class HeroProxyListFactory: ListFactory<HeroProxy>
	{
		public override List<HeroProxy> MakeList(DbContext db)
		{
			List<HeroProxy> heroesList = new List<HeroProxy>();
			HeroBuilder hcb = new HeroBuilder();
			DA_DbContext context = db as DA_DbContext;
			List<HeroModel> heroes = context.Heroes.ToList();
			List<ParametersModel> parameters = context.Parameters.ToList();
			List<PointsModel> points = context.Points.ToList();
			foreach (var x in heroes)
			{
				heroesList.Add(hcb.BuildHeroProxy(x, parameters, points) as HeroProxy);
			}
			return heroesList;
		}
	}
}
