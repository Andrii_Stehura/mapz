﻿using DungeonAdventurer.Entities.Hero_Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Factories
{
	interface IHeroFactory
	{
		Hero CreateHero();
	}
}
