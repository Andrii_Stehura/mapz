﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media;
using DungeonAdventurer.DB.Models;
using DungeonAdventurer.Entities.Item_Entities.Equipment;

namespace DungeonAdventurer.Entities.Hero_Entities
{
	[Serializable]
	class Hero: INotifyPropertyChanged
	{
		private HeroModel heroModel;
		private PointsModel pointsModel;
		private ParametersModel parModel;
		
		[field: NonSerialized]
		public event PropertyChangedEventHandler PropertyChanged;
		public void OnPropertyChanged([CallerMemberName]string prop = "")
		{
			if (PropertyChanged != null)
				PropertyChanged(this, new PropertyChangedEventArgs(prop));
		}

		public int Health
		{
			get { return pointsModel.Health; }
			set 
			{ 
				pointsModel.Health = value;
				OnPropertyChanged();
			}
		}

		public int Armor
		{
			get { return pointsModel.Armor; }
			set 
			{ 
				pointsModel.Armor = value;
				OnPropertyChanged();
			}
		}

		public int Mana
		{
			get { return pointsModel.Mana; }
			set 
			{ 
				pointsModel.Mana = value;
				OnPropertyChanged();
			}
		}

		public int Strength
		{
			get { return parModel.Strength; }
			set 
			{ 
				parModel.Strength = value;
				OnPropertyChanged();
			}
		}

		public int Intelligence
		{
			get { return parModel.Intelligence; }
			set 
			{ 
				parModel.Intelligence = value;
				OnPropertyChanged();
			}
		}

		public int Agility
		{
			get { return parModel.Agility; }
			set 
			{ 
				parModel.Agility = value;
				OnPropertyChanged();
			}
		}

		public HeroClass Class
		{
			get { return heroModel.Class; }
		}

		public HeroRace Race
		{
			get { return heroModel.Race; }
		}

		public string Name
		{
			get { return heroModel.Name; }
		}

		public int Experience
		{
			get { return heroModel.Experience; }
			set
			{
				heroModel.Experience = value;
				if (heroModel.Experience > MaxExperience)
				{
					++heroModel.Level;
					heroModel.Experience = 0;
					OnPropertyChanged("Level");
					OnPropertyChanged("MaxExperience");
				}
				OnPropertyChanged();
			}
		}

		public int MaxExperience
		{
			get
			{
				return heroModel.Level * 100;
			}
		}

		public int Level
		{
			get { return heroModel.Level; }
		}

		public int ID
		{
			get { return (heroModel.Id != 0) ? heroModel.Id : -1; }
		}

		public Hero(HeroModel h, PointsModel pts, ParametersModel par)
		{
			heroModel = h;
			pointsModel = pts;
			parModel = par;
		}

		public Hero(Hero h)
		{
			heroModel = h.heroModel.Clone() as HeroModel;
			pointsModel = h.pointsModel.Clone() as PointsModel;
			parModel = h.parModel.Clone() as ParametersModel;
			heroModel.Id = h.heroModel.Id;
		}
	}
}