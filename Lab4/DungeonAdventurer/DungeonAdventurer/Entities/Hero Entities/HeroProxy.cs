﻿using DungeonAdventurer.DB.Models;
using DungeonAdventurer.Entities.Hero_Entities.Visitor;
using Microsoft.EntityFrameworkCore.Migrations.Operations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Dynamic;
using System.Security.Policy;
using System.Text;

namespace DungeonAdventurer.Entities.Hero_Entities
{
	[Serializable]
	class HeroProxy : Hero, IAttackableHero, IAttacker, ITarget
	{
		private int attack;
		private int basicAttack;
		private int maxArmor;
		private int maxHealth;
		private int maxMana;

		private double attackMultiplier;

		private void RestorePoints()
		{
			Health = maxHealth;
			Armor = maxArmor;
			Mana = maxMana;
		}

		private Parameter GetRaceParameter()
		{
			switch (Race)
			{
				case HeroRace.Elf:
					{
						return Parameter.Agility;
					}
				case HeroRace.Human:
					{
						return Parameter.Intelligence;
					}
				case HeroRace.Orc:
					{
						return Parameter.Strength;
					}
			}
			throw new Exception("Unexpected race");
		}

		private Parameter GetClassParameter()
		{
			switch (Class)
			{
				case HeroClass.Rogue:
					{
						return Parameter.Agility;
					}
				case HeroClass.Mage:
					{
						return Parameter.Intelligence;
					}
				case HeroClass.Fighter:
					{
						return Parameter.Strength;
					}
			}
			throw new Exception("Unexpected class");
		}

		private int GetBasicAttack()
		{
			int ba = 0;
			ba += GetParameterValue(GetRaceParameter());
			ba += GetParameterValue(GetClassParameter());
			return ba;
		}

		private int GetParameterValue(Parameter parameter)
		{
			int res = 0;
			switch (this.GetRaceParameter())
			{
				case Parameter.Agility:
					{
						res = Agility;
						break;
					}
				case Parameter.Intelligence:
					{
						res = Intelligence;
						break;
					}
				case Parameter.Strength:
					{
						res = Strength;
						break;
					}
			}
			return res;
		}

		public static int GetMana(HeroModel h, PointsModel pts, ParametersModel par)
		{
			return (int)(pts.Mana + ProxyMultipliers.GetMultiplier(
					Parameter.Intelligence, h.Class, h.Race) * par.Intelligence);
		}

		public static int GetHealth(HeroModel h, PointsModel pts, ParametersModel par)
		{
			return (int)(pts.Health + ProxyMultipliers.GetMultiplier(
					Parameter.Strength, h.Class, h.Race) * par.Strength);
		}

		public static int GetArmor(HeroModel h, PointsModel pts, ParametersModel par)
		{
			return (int)(pts.Armor + ProxyMultipliers.GetMultiplier(
					Parameter.Agility, h.Class, h.Race) * par.Agility);
		}
		/*
		public static int GetBasicAttack(HeroModel h, PointsModel pts, ParametersModel par)
		{
			return ProxyMultipliers.BASIC_ATTACK + GetClassParameter(h, par) + 
				GetRaceParameter(h, par); 
		}

		public static int GetClassParameter(HeroModel h, ParametersModel par)
		{
			int res = 0;
			switch (h.Class)
			{
				case HeroClass.Fighter:
					{
						res = par.Strength;
						break;
					}
				case HeroClass.Mage:
					{
						res = par.Intelligence;
						break;
					}
				case HeroClass.Rogue:
					{
						res = par.Agility;
						break;
					}
			}
			return res;
		}

		public static int GetRaceParameter(HeroModel h, ParametersModel par)
		{
			int res = 0;
			switch (h.Race)
			{
				case HeroRace.Orc:
					{
						res = par.Strength;
						break;
					}
				case HeroRace.Human:
					{
						res = par.Intelligence;
						break;
					}
				case HeroRace.Elf:
					{
						res = par.Agility;
						break;
					}
			}
			return res;
		}
		*/



		public HeroProxy(HeroModel h, PointsModel pts, ParametersModel par) : base(h, pts, par)
		{
			maxArmor = GetArmor(h, pts, par);
			maxHealth = GetHealth(h, pts, par);
			maxMana = GetMana(h, pts, par);
			RestorePoints();
			attackMultiplier = ProxyMultipliers.GetAttackMultiplier(h.Class, h.Race);
			basicAttack = GetBasicAttack();
		}



		public int Attack 
		{
			get
			{
				return (int)(basicAttack * attackMultiplier) + attack;
			}
		}

		public int MaxArmor
		{
			get
			{
				return maxArmor;
			}
		}
		public int MaxHealth
		{
			get
			{
				return maxHealth;
			}
		}
		public int MaxMana
		{
			get
			{
				return maxMana;
			}
		}

		public void AddExperience(int experience)
		{
			Experience += experience;
		}

		public Visitor.AttackableSlot SlotAttack { get; set; }
		public Visitor.AttackableSlot SlotDefence { get; set; }

		void IAttacker.Attack(HeroProxy h)
		{
			if (h.SlotDefence == this.SlotAttack)
				return;
			else
			{
				int attack = this.Attack;
				int damage = attack;
				damage -= h.Armor;
				if (damage > 0)
				{
					h.Armor = 0;
					h.Health -= damage;
				}
				else
					h.Armor -= attack;
			}
		}

		public void TakeAttack(IAttacker attacker)
		{
			attacker.Attack(this);
		}

		public void Winner()
		{
			Armor = maxArmor; 
			Health += Health / 2;
			if (Health > maxHealth)
				Health = maxHealth;
		}
	}




	static class ProxyMultipliers
	{
		public const double MUL_LOW = 0.5;
		public const double MUL_NORM = 0.75;
		public const double MUL_HIGH = 1.5;
		public const int BASIC_ATTACK = 1;

		public static double GetMultiplier(Parameter p, HeroClass c, HeroRace r)
		{
			switch (p)
			{
				case Parameter.Strength:
					return GetStrMultiplier(c, r);
				case Parameter.Intelligence:
					return GetIntMultiplier(c, r);
				case Parameter.Agility:
					return GetAgiMultiplier(c, r);
			}
			throw new Exception("Not proper parameter.");
		}

		public static double GetStrMultiplier(HeroClass c, HeroRace r)
		{
			double res = 1;
			switch (r)
			{
				case HeroRace.Elf:
					{
						res += MUL_LOW;
						break;
					}
				case HeroRace.Human:
					{
						res += MUL_NORM;
						break;
					}
				case HeroRace.Orc:
					{
						res += MUL_HIGH;
						break;
					}
			}
			switch (c)
			{
				case HeroClass.Mage:
					{
						res += MUL_LOW;
						break;
					}
				case HeroClass.Rogue:
					{
						res += MUL_NORM;
						break;
					}
				case HeroClass.Fighter:
					{
						res += MUL_HIGH;
						break;
					}
			}
			return res;
		}

		public static double GetIntMultiplier(HeroClass c, HeroRace r)
		{
			double res = 1;
			switch (r)
			{
				case HeroRace.Orc:
					{
						res += MUL_LOW;
						break;
					}
				case HeroRace.Human:
					{
						res += MUL_NORM;
						break;
					}
				case HeroRace.Elf:
					{
						res += MUL_HIGH;
						break;
					}
			}
			switch (c)
			{
				case HeroClass.Fighter:
					{
						res += MUL_LOW;
						break;
					}
				case HeroClass.Rogue:
					{
						res += MUL_NORM;
						break;
					}
				case HeroClass.Mage:
					{
						res += MUL_HIGH;
						break;
					}
			}
			return res;
		}

		public static double GetAgiMultiplier(HeroClass c, HeroRace r)
		{
			double res = 1;
			switch (r)
			{
				case HeroRace.Orc:
					{
						res += MUL_LOW;
						break;
					}
				case HeroRace.Human:
					{
						res += MUL_NORM;
						break;
					}
				case HeroRace.Elf:
					{
						res += MUL_HIGH;
						break;
					}
			}
			switch (c)
			{
				case HeroClass.Mage:
					{
						res += MUL_LOW;
						break;
					}
				case HeroClass.Fighter:
					{
						res += MUL_NORM;
						break;
					}
				case HeroClass.Rogue:
					{
						res += MUL_HIGH;
						break;
					}
			}
			return res;
		}

		public static double GetAttackMultiplier(HeroClass c, HeroRace r)
		{
			double res = 0;
			switch (c)
			{
				case HeroClass.Fighter:
					{
						res += MUL_HIGH;
						break;
					}
				case HeroClass.Rogue:
					{
						res += MUL_NORM;
						break;
					}
				case HeroClass.Mage:
					{
						res += MUL_LOW;
						break;
					}
			}
			switch (r)
			{
				case HeroRace.Orc:
					{
						res += MUL_HIGH;
						break;
					}
				case HeroRace.Elf:
					{
						res += MUL_NORM;
						break;
					}
				case HeroRace.Human:
					{
						res += MUL_LOW;
						break;
					}
			}
			return res;
		}
	}
}
