﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Hero_Entities.Visitor
{
	interface IAttacker
	{
		void Attack(HeroProxy h);
	}
}
