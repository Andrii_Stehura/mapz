﻿using System;
using System.Collections.Generic;
using System.Text;
using DungeonAdventurer.Entities.Hero_Entities;

namespace DungeonAdventurer.Entities.Item_Entities.Equipment
{
	abstract class Modifiable
	{
		//values
		protected int value;

		//methods
		public Modifiable(int value)
		{
			this.value = value;
		}

		public abstract void Modify(Hero hero);
		public abstract void Demodify(Hero hero);
	}
}
