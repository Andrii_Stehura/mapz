﻿using DungeonAdventurer.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace DungeonAdventurer.Entities.Level_Entities.Battle_Level
{
	[Serializable]
	class BattleLevel : CellStateChanger, ILevel
	{
		public void Execute()
		{
			MessageBox.Show("Battle!");
			ViewsRouter.Instance.RouteTo(Routes.Battle);
			ChangeCellState();
		}
	}
}
