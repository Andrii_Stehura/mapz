﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;

namespace DungeonAdventurer.Entities.Level_Entities.Map_Level.CellStates
{
	interface ICellState
	{
		public Brush GetStateColor();
	}
}
