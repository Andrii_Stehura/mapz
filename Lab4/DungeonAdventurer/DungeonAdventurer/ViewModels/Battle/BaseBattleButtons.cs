﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Media;

namespace DungeonAdventurer.ViewModels.Battle
{
	abstract class BaseBattleButtons: BaseViewModel, IUpdateProperties
	{
		private BattleButton choice;

		private BattleButton StrToButton(string str)
		{
			switch (str)
			{
				case ("head"):
					{
						return BattleButton.Head;
					}
				case ("body"):
					{
						return BattleButton.Body;
					}
				case ("legs"):
					{
						return BattleButton.Legs;
					}
			}
			return BattleButton.None;
		}

		abstract public SolidColorBrush GetBackground();

		public BaseBattleButtons()
		{
			choice = BattleButton.None;
		}

		public BattleButton Choice
		{
			get { return choice; }
			set { choice = value; }
		}

		public DelegateCommand ButtonClick
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					string par = obj as string;
					switch (par)
					{
						case ("head"):
							{
								if (choice != BattleButton.Head)
									choice = BattleButton.Head;
								else
									choice = BattleButton.None;
								break;
							}
						case ("body"):
							{
								if (choice != BattleButton.Body)
									choice = BattleButton.Body;
								else
									choice = BattleButton.None;
								break;
							}
						case ("legs"):
							{
								if (choice != BattleButton.Legs)
									choice = BattleButton.Legs;
								else
									choice = BattleButton.None;
								break;
							}
					}
				}, (obj)=>
				{
					bool res = false;
					if (choice == BattleButton.None)
						res = true;
					
					else if (choice == StrToButton((string)obj))
						res = true;

					return res;
				});
			}
		}

		public void UpdateProperties()
		{
			OnPropertyChanged("Choice");	
		}
	}
}
