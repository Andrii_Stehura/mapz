﻿using DungeonAdventurer.Entities.Factories;
using DungeonAdventurer.Entities.Hero_Entities;
using DungeonAdventurer.Entities.Hero_Entities.Visitor;
using DungeonAdventurer.Entities.Level_Entities.Level_end_strategies;
using DungeonAdventurer.ViewModels.Battle;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace DungeonAdventurer.ViewModels
{
	class BattleViewModel: BaseViewModel, IUpdateProperties
	{
		private BaseBattleButtons attackBtnContext;
		private BaseBattleButtons defenceBtnContext;
		private HeroProxy hero, enemy;
		private ILevelEndStrategy end;

		private void SetHeroSlots()
		{
			switch (attackBtnContext.Choice)
			{
				case (BattleButton.Head):
					{
						hero.SlotAttack = AttackableSlot.Head;
						break;
					}
				case (BattleButton.Body):
					{
						hero.SlotAttack = AttackableSlot.Body;
						break;
					}
				case (BattleButton.Legs):
					{
						hero.SlotAttack = AttackableSlot.Legs;
						break;
					}
			}

			switch (defenceBtnContext.Choice)
			{
				case (BattleButton.Head):
					{
						hero.SlotDefence = AttackableSlot.Head;
						break;
					}
				case (BattleButton.Body):
					{
						hero.SlotDefence = AttackableSlot.Body;
						break;
					}
				case (BattleButton.Legs):
					{
						hero.SlotDefence = AttackableSlot.Legs;
						break;
					}
			}
		}

		private void SetEnemySlots()
		{
			const double CHANCE_LOW = 0.3;
			const double CHANCE_MED = 0.6;
			//else is high chance

			Random rand = new Random();
		 	double r = rand.NextDouble();
			if (r <= CHANCE_LOW)
				enemy.SlotAttack = AttackableSlot.Head;
			else if (r <= CHANCE_MED)
				enemy.SlotAttack = AttackableSlot.Body;
			else
				enemy.SlotAttack = AttackableSlot.Legs;

			r = rand.NextDouble();
			if (r <= CHANCE_LOW)
				enemy.SlotDefence = AttackableSlot.Head;
			else if (r <= CHANCE_MED)
				enemy.SlotDefence = AttackableSlot.Body;
			else
				enemy.SlotDefence = AttackableSlot.Legs;
		}

		public BattleViewModel()
		{
			attackBtnContext = new AttackButtons();
			defenceBtnContext = new DefenceButtons();
			hero = ViewsRouter.Instance.Context.GetContext("hero") as HeroProxy;
			IHeroFactory factory = new EnemyFactory(hero.Level);
			enemy = factory.CreateHero() as HeroProxy;
		}

		public BaseBattleButtons AttackBtnsContex { get { return attackBtnContext; } }
		public BaseBattleButtons DefenceBtnsContex { get { return defenceBtnContext; } }
		public HeroProxy Hero { get { return hero; } }
		public HeroProxy Enemy { get { return enemy; } }
		public DelegateCommand MenuClick
		{
			get
			{
				return new DelegateCommand((obj) => {
					ViewsRouter.Instance.Context.Clear();
					ViewsRouter.Instance.RouteTo(Routes.MainMenu);
				});
			}
		}

		public DelegateCommand FightClick
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					SetHeroSlots();
					SetEnemySlots();
					enemy.TakeAttack(hero);
					if (enemy.Health <= 0)
					{
						MessageBox.Show("You won!");
						end = new WinLevelStrategy(hero, enemy.Experience);
						end.EndLevel();
					}
					else
					{
						hero.TakeAttack(enemy);
						if (hero.Health <= 0)
						{
							end = new LooseLevelStrategy();
							end.EndLevel();
						}
					}
					UpdateProperties();

				},(obj)=>
				{
					bool res = false;
					if (attackBtnContext.Choice != BattleButton.None
						&& defenceBtnContext.Choice != BattleButton.None)
							res = true;
					return res;
				});
			}
		}
		public void UpdateProperties()
		{
			OnPropertyChanged("Hero");
			OnPropertyChanged("Enemy");
			OnPropertyChanged("AttackBtnsContex");
			OnPropertyChanged("DefenceBtnsContex");
		}
	}
}
