﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.ViewModels
{
	interface IUpdateProperties
	{
		public void UpdateProperties();
	}
}
