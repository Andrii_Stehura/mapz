﻿using System.Windows.Controls;

namespace DungeonAdventurer.ViewModels
{
	class MainViewModel : BaseViewModel
	{
		private UserControl currentPage;

		public UserControl CurrentPage 
		{ 
			get { return currentPage; } 
			set 
			{ 
				currentPage = value;
				OnPropertyChanged();
			} 
		}
		public MainViewModel():base()
		{
			ViewsRouter.Instance.MainVM = this;
			ViewsRouter.Instance.RouteTo(Routes.MainMenu);
		}
	}
}
