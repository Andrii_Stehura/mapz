﻿using DungeonAdventurer.Entities.Factories;
using DungeonAdventurer.Entities.Level_Entities;
using DungeonAdventurer.Entities.Level_Entities.Map_Level;
using DungeonAdventurer.Entities.Level_Entities.Map_Level.CellStates;
using DungeonAdventurer.Views.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls.Primitives;

namespace DungeonAdventurer.ViewModels
{
	[Serializable]
	class MapViewModel: BaseViewModel, IUpdateProperties
	{
		private int rows;
		private int cols;

		private MapCell[][] mapCells;

		public MapViewModel()
		{
			rows = 3;
			cols = 4;
			ArrayFactory<MapCell> factory = new MapCellsArrayFactory();
			mapCells = factory.MakeArray(rows, cols);
			mapCells[0][0].ChangeState(new ChooseableCellState());
		}

		public List<MapCell> Cells 
		{
			get 
			{
				var list = new List<MapCell>();
				foreach (var x in mapCells)
					foreach (var y in x)
						list.Add(y);
				return list;
			}
		}

		public int Rows
		{
			get { return rows; }
		}

		public int Columns
		{
			get { return cols; }
		}

		public void UpdateProperties()
		{
			MapCell cell = ViewsRouter.Instance.Context.GetContext("cell") 
				as MapCell;

			if (cell != null)
			{
				UnlockCells(cell.Row, cell.Column);
				OnPropertyChanged("Cells");
				ViewsRouter.Instance.Context.Remove("cell");
			}
		}

		private void UnlockCells(int baseRow, int baseColumn)
		{
			int unlockRow, unlockCol;
			ICellState chooseable = new ChooseableCellState();

			//left
			unlockRow = baseRow;
			unlockCol = baseColumn - 1;
			if (unlockCol >= 0)
				if(mapCells[unlockRow][unlockCol].CurrentState is UnableCellState)
					mapCells[unlockRow][unlockCol].ChangeState(chooseable);

			//top
			unlockRow = baseRow - 1;
			unlockCol = baseColumn;
			if (unlockRow >= 0)
				if (mapCells[unlockRow][unlockCol].CurrentState is UnableCellState)
					mapCells[unlockRow][unlockCol].ChangeState(chooseable);

			//right
			unlockRow = baseRow;
			unlockCol = baseColumn + 1;
			if(unlockCol < cols)
				if (mapCells[unlockRow][unlockCol].CurrentState is UnableCellState)
					mapCells[unlockRow][unlockCol].ChangeState(chooseable);

			//bottom
			unlockRow = baseRow + 1;
			unlockCol = baseColumn;
			if(unlockRow < rows)
				if (mapCells[unlockRow][unlockCol].CurrentState is UnableCellState)
					mapCells[unlockRow][unlockCol].ChangeState(chooseable);
		}
	}
}
