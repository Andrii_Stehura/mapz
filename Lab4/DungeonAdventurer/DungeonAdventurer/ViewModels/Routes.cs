﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.ViewModels
{
	enum Routes
	{
		MainMenu, Play, Heroes, CreateHero, Map, Prev,
		Battle, Load
	}
}
