﻿using DungeonAdventurer.DB;
using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.ViewModels
{
	[Serializable]
	class ViewModelContext
	{
		private Dictionary<string, object> context;

		public ViewModelContext()
		{
			context = new Dictionary<string, object>();
		}

		public object GetContext(string key)
		{
			if (context.ContainsKey(key))
				return context[key];

			return null;
		}

		public void SetContext(string key, object obj)
		{
			if (context.ContainsKey(key))
				context.Remove(key);

			context.Add(key, obj);
		}

		public void Clear()
		{
			context.Clear();
		}

		public void Remove(string key)
		{
			if(context.ContainsKey(key))
				context.Remove(key);
		}

		public void Restore(IViewModelContextSave save)
		{
			context = save.Context.context;
		}
	}
}
