﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.DB.Adapters
{
	abstract class DbAdapter
	{
		protected DA_DbContext db;
		public DbAdapter(DbContext dc)
		{
			db = dc as DA_DbContext;
		}

		abstract public void Save(object obj);
		abstract public void Remove(object obj);
	}
}
