﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;
using DungeonAdventurer.Entities.Hero_Entities;
using DungeonAdventurer.DB.Models;

namespace DungeonAdventurer.DB
{
	class DA_DbContext: DbContext
	{
		public DA_DbContext()
		{
			Database.EnsureCreated();
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseMySql("server=localhost;UserId=root;Password=MYSQLroot;database=dungeon_adventurer_db;");
		}

		public DbSet<HeroModel> Heroes { get; set; }
		public DbSet<EquipmentModel> Equipment { get; set; }
		public DbSet<PointsModel> Points { get; set; }
		public DbSet<ParametersModel> Parameters { get; set; }
	}
}
