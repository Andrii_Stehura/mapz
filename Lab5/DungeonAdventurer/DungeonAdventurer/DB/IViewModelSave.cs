﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.DB
{
	interface IViewModelSave
	{
		DateTime Date { get; }
		string Name { get; }
	}
}
