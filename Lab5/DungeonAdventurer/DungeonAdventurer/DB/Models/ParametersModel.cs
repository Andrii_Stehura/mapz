﻿using DungeonAdventurer.Entities.Hero_Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.DB.Models
{
	[Serializable]
	class ParametersModel: ICloneable
	{
		public int Id { get; set; }
		public int Strength { get; set; }
		public int Intelligence { get; set; }
		public int Agility { get; set; }

		public HeroModel Hero { get; set; }

		public ParametersModel()
		{
			Strength = 1;
			Intelligence = 1;
			Agility = 1;
		}

		public ParametersModel(ParametersModel pm)
		{
			this.Strength = pm.Strength;
			this.Intelligence = pm.Intelligence;
			this.Agility = pm.Agility;
		}

		public ParametersModel(Hero h)
		{
			Intelligence = h.Intelligence;
			Strength = h.Strength;
			Agility = h.Agility;
		}

		public object Clone()
		{
			return new ParametersModel(this);
		}
	}
}
