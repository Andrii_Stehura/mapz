﻿using System;
using System.Collections.Generic;
using System.Text;
using DungeonAdventurer.DB;
using DungeonAdventurer.DB.Models;
using DungeonAdventurer.Entities.Hero_Entities;
using System.Linq;
using DungeonAdventurer.Views;
using DungeonAdventurer.Entities.Builders.RandomModelBuilders;
using DungeonAdventurer.Entities.Builders.Random_Model_Builders;

namespace DungeonAdventurer.Entities.Builders
{
	class HeroBuilder
	{
		public Hero BuildHero(HeroModel h, List<ParametersModel> pm, List<PointsModel> pom)
		{
			return new Hero(h, pom.Single((x) => x.Hero.Id == h.Id),
				pm.Single((x) => x.Hero.Id == h.Id));
		}

		public Hero BuildHeroProxy(int lvl)
		{
			IRandomModelBuilder randomModelBuilder = new RandomHeroModelBuilder(lvl);
			HeroModel h = randomModelBuilder.BuildModel() as HeroModel;
			randomModelBuilder = new RandomParametersModelBuilder(lvl);
			ParametersModel par = randomModelBuilder.BuildModel() as ParametersModel;
			par.Hero = h;
			PointsModel pts = new PointsModel();
			pts.Hero = h;
			return new HeroProxy(h, pts, par); 
		}

		public Hero BuildHeroProxy(HeroModel h, List<ParametersModel> pm, List<PointsModel> pom)
		{
			return new HeroProxy(h, pom.Single((x) => x.Hero.Id == h.Id),
				pm.Single((x) => x.Hero.Id == h.Id));
		}
	}
}
