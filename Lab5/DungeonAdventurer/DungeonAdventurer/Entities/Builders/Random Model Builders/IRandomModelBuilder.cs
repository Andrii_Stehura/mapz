﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Builders.RandomModelBuilders
{
	interface IRandomModelBuilder
	{
		object BuildModel();
	}
}
