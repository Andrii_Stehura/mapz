﻿using DungeonAdventurer.DB.Models;
using DungeonAdventurer.Entities.Builders.RandomModelBuilders;
using DungeonAdventurer.Entities.Hero_Entities;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace DungeonAdventurer.Entities.Builders.Random_Model_Builders
{
	class RandomHeroModelBuilder : IRandomModelBuilder
	{
		private const double LOW_CHANCE = 0.3;
		private const double MED_CHANCE = 0.6;
		//else is high chance

		private const int EXP_MULTIPLIER = 25;
		private int lvl;

		private HeroClass SetClass()
		{
			Random random = new Random();
			double r = random.NextDouble();
			if (r <= LOW_CHANCE)
				return HeroClass.Mage;

			if (r <= MED_CHANCE)
				return HeroClass.Fighter;

			return HeroClass.Rogue;
		}

		private HeroRace SetRace()
		{
			Random random = new Random();
			double r = random.NextDouble();
			if (r <= LOW_CHANCE)
				return HeroRace.Orc;

			if (r <= MED_CHANCE)
				return HeroRace.Elf;

			return HeroRace.Human;
		}

		public RandomHeroModelBuilder(int level)
		{
			lvl = level;
		}

		public object BuildModel()
		{
			HeroModel h = new HeroModel();
			if(lvl > 1)
				h.Level = lvl;
			h.Class = SetClass();
			h.Race = SetRace();
			h.Experience = lvl * EXP_MULTIPLIER;
			return h;
		}
	}
}
