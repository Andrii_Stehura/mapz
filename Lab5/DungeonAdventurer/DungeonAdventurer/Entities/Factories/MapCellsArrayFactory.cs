﻿using DungeonAdventurer.Entities.Level_Entities.Map_Level;

namespace DungeonAdventurer.Entities.Factories
{
	class MapCellsArrayFactory : ArrayFactory<MapCell>
	{
		public override MapCell[] MakeArray(int columns)
		{
			MapCell[] arr = new MapCell[columns];
			for (int i = 0; i < columns; ++i)
			{
				arr[i] = new MapCell();
				arr[i].Column = i;
			}
			return arr;
		}

		public override MapCell[][] MakeArray(int rows, int columns)
		{
			MapCell[][] arr = new MapCell[rows][];
			for (int i = 0; i < rows; i++)
			{
				arr[i] = MakeArray(columns);
				for (int j = 0; j < columns; ++j)
					arr[i][j].Row = i;
			}
			return arr;
		}
	}
}
