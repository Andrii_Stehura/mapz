﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Hero_Entities
{
	public class HeroPoints: ICloneable
	{
		public int Health { get; set; }
		public int Armor { get; set; }
		public int Mana { get; set; }

		public HeroPoints()
		{
			Health = 0;
			Armor = 0;
			Mana = 0;
		}

		public HeroPoints(int health, int armor, int mana)
		{
			Health = health;
			Armor = armor;
			Mana = mana;
		}

		public HeroPoints(HeroPoints h)
		{
			Health = h.Health;
			Armor = h.Armor;
			Mana = h.Mana;
		}

		public object Clone()
		{
			return new HeroPoints(this);
		}
	}
}
