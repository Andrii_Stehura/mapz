﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Hero_Entities.Visitor
{
	interface IAttackableHero
	{
		AttackableSlot SlotAttack { get; set; }
		AttackableSlot SlotDefence { get; set; }

	}
}
