﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Hero_Entities.Visitor
{
	interface ITarget
	{
		void TakeAttack(IAttacker attacker);
	}
}
