﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Item_Entities.Equipment
{
	public enum EquipmentSlot
	{
		Head, Body, Legs, Weapon
	}
}
