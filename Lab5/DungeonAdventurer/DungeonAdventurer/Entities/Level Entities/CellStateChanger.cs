﻿using DungeonAdventurer.Entities.Level_Entities.Map_Level;
using DungeonAdventurer.Entities.Level_Entities.Map_Level.CellStates;
using DungeonAdventurer.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Level_Entities
{
	[Serializable]
	abstract class CellStateChanger
	{
		protected void ChangeCellState()
		{
			MapCell cell = ViewsRouter.Instance.Context.GetContext("cell") as MapCell;
			cell.ChangeState(new ObservedCellState());
		}
	}
}
