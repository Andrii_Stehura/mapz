﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Level_Entities.Level_end_strategies
{
	interface ILevelEndStrategy
	{
		void EndLevel();
	}
}
