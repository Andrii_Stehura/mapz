﻿using DungeonAdventurer.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace DungeonAdventurer.Entities.Level_Entities.Level_end_strategies
{
	[Serializable]
	class LooseLevelStrategy : ILevelEndStrategy
	{
		public void EndLevel()
		{
			MessageBox.Show("You lost!");
			ViewsRouter.Instance.Context.Clear();
			ViewsRouter.Instance.RouteTo(Routes.MainMenu);
		}
	}
}
