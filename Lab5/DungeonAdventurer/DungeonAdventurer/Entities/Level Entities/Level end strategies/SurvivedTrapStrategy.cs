﻿using DungeonAdventurer.Entities.Hero_Entities;
using DungeonAdventurer.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Level_Entities.Level_end_strategies
{
	[Serializable]
	class SurvivedTrapStrategy : ILevelEndStrategy
	{
		private HeroProxy hero;
		private int experience;

		public SurvivedTrapStrategy(HeroProxy h, int exp)
		{
			hero = h;
			experience = exp;
		}

		public void EndLevel()
		{
			hero.AddExperience(experience);
			ViewsRouter.Instance.RouteTo(Routes.Map);
		}
	}
}
