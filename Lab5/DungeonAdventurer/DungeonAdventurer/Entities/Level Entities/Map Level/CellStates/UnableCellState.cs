﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;

namespace DungeonAdventurer.Entities.Level_Entities.Map_Level.CellStates
{
	[Serializable]
	class UnableCellState : ICellState
	{
		public Brush GetStateColor()
		{
			return Brushes.Gray;
		}
	}
}
