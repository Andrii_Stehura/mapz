﻿using DungeonAdventurer.Entities.Level_Entities.Battle_Level;
using DungeonAdventurer.Entities.Level_Entities.Map_Level.CellStates;
using DungeonAdventurer.Entities.Level_Entities.Trap_Level;
using DungeonAdventurer.Entities.Level_Entities.Treasure_Level;
using DungeonAdventurer.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Media;

namespace DungeonAdventurer.Entities.Level_Entities.Map_Level
{
	[Serializable]
	class MapCell: INotifyPropertyChanged
	{
		private ILevel cellLevel;
		private ICellState state;
		private int row, col;

		private ILevel GetRandomLevel()
		{
			const double TRAP_CHANCE = 0.2;
			const double TREASURE_CHANCE = 0.45;
			Random r = new Random();
			double chance = r.NextDouble();
			if (chance <= TRAP_CHANCE)
				return new TrapLevel();
			else
			{
				if (chance <= TREASURE_CHANCE)
					return new TreasureLevel();
			}
			return new BattleLevel();
		}
		
		[field: NonSerialized]
		public event PropertyChangedEventHandler PropertyChanged;
		public void OnPropertyChanged([CallerMemberName]string prop = "")
		{
			if (PropertyChanged != null)
				PropertyChanged(this, new PropertyChangedEventArgs(prop));
		}


		public MapCell()
		{
			state = new UnableCellState();
			cellLevel = GetRandomLevel();
		}

		public MapCell(int Row, int Col):this()
		{
			row = Row;
			col = Col;
		}

		public ICellState CurrentState
		{ get { return state; } }

		public Brush Color 
		{
			get { return state.GetStateColor(); } 
		}

		public int Row 
		{
			get { return row; }
			set 
			{ 
				row = value;
				OnPropertyChanged();
			} 
		}

		public int Column
		{
			get { return col; }
			set
			{
				col = value;
				OnPropertyChanged();
			}
		}

		public DelegateCommand ExecuteLevel
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					ViewsRouter.Instance.Context.SetContext("cell", this);
					cellLevel.Execute();
				}, 
				(obj)=> 
				{
					bool canExecute = false;
					if (state is ChooseableCellState)
						canExecute = true;
					return canExecute;
				});
			}
		}

		public void ChangeState(ICellState newState)
		{
			state = newState;
			OnPropertyChanged("Color");
		}
	}
}
