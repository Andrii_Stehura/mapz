﻿using DungeonAdventurer.Entities.Hero_Entities;
using DungeonAdventurer.Entities.Hero_Entities.Visitor;
using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.Entities.Level_Entities.Trap_Level
{
	[Serializable]
	class Trap : IAttacker
	{
		private const double CHANCE_LOW = 0.2;
		private const double CHANCE_MED = 0.5;
		//else is high

		private const double DMG_LOW = 0.1;
		private const double DMG_MED = 0.2;
		private const double DMG_HIGH = 0.3;

		public void Attack(HeroProxy h)
		{
			double r = (new Random()).NextDouble();

			double damage;
			if (h.Armor > 0)
				damage = h.MaxArmor;
			else
				damage = h.MaxHealth;

			if (r <= CHANCE_LOW)
				damage *= DMG_HIGH;
			else if (r <= CHANCE_MED)
				damage *= DMG_MED;
			else
				damage *= DMG_LOW;

			if ((damage - h.Armor) < 0)
				h.Armor -= (int)damage;
			else
			{
				h.Armor = 0;
				h.Health -= (int)damage;
			}
		}

		public int EarnExperience(HeroProxy h)
		{
			return (int)(h.MaxExperience * 0.25);
		}
	}
}
