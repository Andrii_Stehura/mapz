﻿using DungeonAdventurer.Entities.Hero_Entities;
using DungeonAdventurer.Entities.Level_Entities.Level_end_strategies;
using DungeonAdventurer.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace DungeonAdventurer.Entities.Level_Entities.Trap_Level
{
	[Serializable]
	class TrapLevel : CellStateChanger, ILevel
	{
		private Trap trap;
		private ILevelEndStrategy end;

		public TrapLevel()
		{
			trap = new Trap();
		}
		public void Execute()
		{
			MessageBox.Show("It's a trap!");
			ChangeCellState();
			HeroProxy hero = ViewsRouter.Instance.Context.GetContext("hero")
				as HeroProxy;
			hero.TakeAttack(trap);
			if (hero.Health <= 0)
				end = new LooseLevelStrategy();
			else
				end = new SurvivedTrapStrategy(hero, trap.EarnExperience(hero));
			end.EndLevel();
		}
	}
}
