﻿using DungeonAdventurer.Entities.Hero_Entities;
using DungeonAdventurer.Entities.Level_Entities.Level_end_strategies;
using DungeonAdventurer.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace DungeonAdventurer.Entities.Level_Entities.Treasure_Level
{
	[Serializable]
	class TreasureLevel : CellStateChanger, ILevel
	{
		ILevelEndStrategy end;
		public void Execute()
		{
			MessageBox.Show("Treasure! Yessss!");
			ChangeCellState();

			HeroProxy hero = ViewsRouter.Instance.Context.GetContext("hero") 
				as HeroProxy;

			end = new WinLevelStrategy(hero, (int)(hero.MaxExperience * 0.35));
			end.EndLevel();
		}
	}
}
