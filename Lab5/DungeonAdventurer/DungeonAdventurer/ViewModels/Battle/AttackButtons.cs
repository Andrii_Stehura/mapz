﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;

namespace DungeonAdventurer.ViewModels.Battle
{
	class AttackButtons : BaseBattleButtons
	{
		public override SolidColorBrush GetBackground()
		{
			return Brushes.Tomato;	
		}

		public SolidColorBrush Color { get { return GetBackground(); } }
	}
}
