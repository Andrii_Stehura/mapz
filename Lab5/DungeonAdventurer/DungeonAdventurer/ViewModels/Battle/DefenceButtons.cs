﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;

namespace DungeonAdventurer.ViewModels.Battle
{
	class DefenceButtons : BaseBattleButtons
	{
		public override SolidColorBrush GetBackground()
		{
			return Brushes.LightBlue;
		}

		public SolidColorBrush Color { get { return GetBackground(); } }
	}
}
