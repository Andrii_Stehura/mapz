﻿using DungeonAdventurer.DB;
using DungeonAdventurer.Entities.Factories;
using DungeonAdventurer.Entities.Hero_Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace DungeonAdventurer.ViewModels
{
	class ChooseHeroViewModel: BaseViewModel, IUpdateProperties
	{
		private List<HeroProxy> heroesList;

		public List<HeroProxy> HeroesList
		{
			get 
			{
				App.Current.MainWindow.Cursor = Cursors.Wait;
				ListFactory<HeroProxy> hclf = new HeroProxyListFactory();
				heroesList = hclf.MakeList(new DA_DbContext());
				App.Current.MainWindow.Cursor = Cursors.Arrow;
				return heroesList; 
			}
		}

		public DelegateCommand MenuClick
		{
			get
			{
				return new DelegateCommand((obj) => {
					ViewsRouter.Instance.Context.Clear();
					ViewsRouter.Instance.RouteTo(Routes.MainMenu);
				});
			}
		}

		public DelegateCommand CreateClick
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					ViewsRouter.Instance.RouteTo(Routes.CreateHero);
				});
			}
		}

		public DelegateCommand PlayClick
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					int id = (int)obj;
					ViewsRouter.Instance.Context.SetContext("hero", heroesList.Find((x)=> x.ID == id));
					ViewsRouter.Instance.RouteTo(Routes.Load);
				});
			}
		}

		public void UpdateProperties()
		{
			OnPropertyChanged("HeroesList");
		}
	}
}
