﻿using DungeonAdventurer.DB;
using DungeonAdventurer.Entities.Hero_Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonAdventurer.ViewModels
{
	class LoadGameViewModel:BaseViewModel
	{
		Hero hero;
		public LoadGameViewModel()
		{
			hero = ViewsRouter.Instance.Context.GetContext("hero") as Hero;
		}

		public DelegateCommand ContinueClick 
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					ViewsRouter.Instance.Context.SetContext("new map", true);
					ViewsRouter.Instance.Context.SetContext("restore", hero.Name);
					ViewsRouter.Instance.RouteTo(Routes.Map);
				}, (obj) =>
				{
					return SaveSerializer.CheckName(hero.Name);
				});
			}
		}

		public DelegateCommand NewClick
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					ViewsRouter.Instance.Context.SetContext("new map", true);
					ViewsRouter.Instance.RouteTo(Routes.Map);
				});
			}
		}

		public DelegateCommand BackClick
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					ViewsRouter.Instance.Context.Clear();
					ViewsRouter.Instance.RouteTo(Routes.Prev);
				});
			}
		}
	}
}
