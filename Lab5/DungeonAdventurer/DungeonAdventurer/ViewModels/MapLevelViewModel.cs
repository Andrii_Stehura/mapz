﻿using DungeonAdventurer.DB;
using DungeonAdventurer.DB.Adapters;
using DungeonAdventurer.Entities.Hero_Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace DungeonAdventurer.ViewModels
{
	class MapLevelViewModel: BaseViewModel, IUpdateProperties
	{
		private HeroProxy hero;
		private IUpdateProperties mapContext;

		public MapLevelViewModel()
		{
			string name = ViewsRouter.Instance.Context.GetContext("restore") 
				as string;
			if (name != null)
			{
				SaveSerializer s = new SaveSerializer();
				var save = s.Deserialize(name);
				if (save == null)
				{
					ViewsRouter.Instance.RouteTo(Routes.MainMenu);
					ViewsRouter.Instance.Context.Clear();
					return;
				}
				Restore(save);
			}
			else
			{
				mapContext = new MapViewModel();
				hero = (HeroProxy)ViewsRouter.Instance.Context.GetContext("hero");
			}
			ViewsRouter.Instance.Context.SetContext("new map", false);

		}

		public IUpdateProperties MapContext
		{ get { return mapContext; } }

		public HeroProxy Hero
		{
			get
			{
				return hero;
			}
		}

		public IViewModelSave Save()
		{
			return new MapSave(this);
		}

		public void Restore(IViewModelSave save)
		{
			if (save is MapSave mapSave)
			{
				hero = mapSave.Hero;
				mapContext = mapSave.MapContext;
				ViewsRouter.Instance.Context.Restore(mapSave);
			}
			else
				throw (new ArgumentException($"Can not restore {this.GetType().Name} " +
					$"from {save.GetType().Name}."));
		}

		public DelegateCommand MenuClick
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					ViewsRouter.Instance.RouteTo(Routes.MainMenu);
				});
			}
		}

		public DelegateCommand SaveClick
		{
			get
			{
				return new DelegateCommand((obj) =>
				{
					SaveSerializer s = new SaveSerializer();
					s.SerializeAndSave(this.Save());
				});
			}
		}

		public void UpdateProperties()
		{
			OnPropertyChanged("Hero");
			mapContext.UpdateProperties();
		}
	}
}
