﻿using DungeonAdventurer.Entities.Hero_Entities;
using DungeonAdventurer.Views;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace DungeonAdventurer.ViewModels
{
	class ViewsRouter
	{
		private static ViewsRouter instance;
		private static MainViewModel mainVM;
		private static ViewModelContext context;
		private Stack<UserControl> history;

		private UserControl menu;
		private UserControl heroesView;
		private UserControl chooseHero;
		private UserControl mapLevel;
		
		private ViewsRouter()
		{
			history = new Stack<UserControl>();
		}

		public MainViewModel MainVM { get { return mainVM; } set { mainVM = value; } }

		static public ViewsRouter Instance
		{
			get
			{
				return instance ?? (instance = new ViewsRouter());
			}
		}

		public ViewModelContext Context
		{
			get
			{
				return context ?? (context = new ViewModelContext());
			}
		}

		public void RouteTo(Routes route)
		{
			history.Push(mainVM.CurrentPage);
			bool Created = false;
			switch (route)
			{
				case (Routes.MainMenu):
					{
						if (menu == null)
						{
							menu = new MainMenu();
							Created = true;
						}
						mainVM.CurrentPage = menu;
						break;
					}
				case (Routes.Play):
					{
						if (chooseHero == null)
						{
							chooseHero = new ChooseHero();
							Created = true;
						}
						mainVM.CurrentPage = chooseHero;
						break;
					}
				case (Routes.Heroes):
					{
						if (heroesView == null)
						{
							heroesView = new HeroesView();
							Created = true;
						}
						mainVM.CurrentPage = heroesView; 
						break;
					}
				case (Routes.CreateHero):
					{
						mainVM.CurrentPage = new CreateHero();
						Created = true;
						break;
					}
				case (Routes.Map):
					{
						if ((bool)context.GetContext("new map") || mapLevel == null)
						{
							mapLevel = new MapLevel();
							Created = true;
						}
						mainVM.CurrentPage = mapLevel;
						break;
					}
				case (Routes.Battle):
					{
						mainVM.CurrentPage = new Views.Battle();
						Created = true;
						break;
					}
				case (Routes.Load):
					{
						mainVM.CurrentPage = new LoadGame();
						Created = true;
						break;
					}
				case (Routes.Prev):
					{
						history.Pop();
						mainVM.CurrentPage = history.Pop();
						break;
					}
			}
			if (mainVM.CurrentPage.DataContext is IUpdateProperties u && !Created)
				u.UpdateProperties();
		}
	}
}
